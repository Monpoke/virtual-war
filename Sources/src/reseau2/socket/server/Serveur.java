package reseau2.socket.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import outils.InfoTransfert;
import outils.Textes;
import reseau2.socket.client.ReseauExecuteur;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.robots.Robot;

/**
 * Classe représentant le serveur.
 *
 * @author Pierre
 */
public class Serveur implements Runnable {

    private ServerSocket ss;
    private Thread t;
    private final int PORT;
    private boolean estLance = false;

    Map<Socket, Client> listeClients;

    public Serveur(int PORT) {
        this.PORT = PORT;

        listeClients = new HashMap<>();
    }

    @Override
    public void run() {

        try {
            ss = new ServerSocket(PORT);
            Textes.printErr("Le serveur est à l'écoute du port " + ss.getLocalPort());

            t = new Thread(new ConnexionClient(ss, this), "AccepterUnClient");
            t.start();

        } catch (IOException e) {
            Textes.printErr("Le port " + PORT + " est déjà utilisé !");
        }

    }

    /**
     * Fonction publique pour lancer le serveur.
     */
    public void lancerServeur() {
        Thread tServeur;
        tServeur = new Thread(this, "ThreadServeur");
        tServeur.start();
        estLance = true;
    }

    /**
     * Retourne vrai si le serveur est fonctionnel.
     *
     * @return
     */
    public boolean estFonctionnel() {
        return this.estLance;
    }

    /**
     *
     * @return le nombre de client connectés.
     */
    public int getNombreConnectes() {
        return listeClients.size();
    }

    /**
     * Retourne le nombre d'équipe externe qui sont vraiment prêts. (Noms saisis
     * + équipe constituée)
     *
     * @return le nombre d'équipe ok.
     */
    public int getNombreOperationnel() {
        int nb = 0;

        for (Map.Entry<Socket, Client> entrySet : listeClients.entrySet()) {
            nb += (entrySet.getValue().estOperationnel()) ? 1 : 0;
        }

        return nb;
    }

    /**
     * Enregistre un nouveau client.
     *
     * @param socket
     * @param client
     */
    public void nouveauClient(Socket socket, Client client) {
        listeClients.put(socket, client);
    }

    /**
     * Déconnecte un client.
     *
     * @param socket
     */
    public void deconnexionClient(Socket socket) {
        listeClients.remove(socket);
    }

    /**
     * On notifie le serveur d'une info
     *
     * @param infos
     */
    public void notifierInfos(InfoTransfert infos) {
        /**
         * On envoie les données pour les adversaires particuliers
         */
        List<String> typesAEnvoyer = new ArrayList<>();
        typesAEnvoyer.add("deplacement");

        // On répercute sur le réseau
        if (typesAEnvoyer.contains(infos.getType())) {
            Textes.printErr("DONNEES A ENVOYER !!!!");
            for (Map.Entry<Socket, Client> entrySet : listeClients.entrySet()) {
                // on vérifie que ce n'est pas l'émetteur
                if (!entrySet.getValue().equals(infos.getClientOriginal())) {
                    Textes.printErr("ON ENVOIE AU CLIENT " + entrySet.getValue().getNumeroEquipe());
                    entrySet.getValue().transfertInfos(infos);
                }
            }

            // On répercute sur le local
            if (!infos.fromLocal()) {
                ReseauExecuteur.execute(infos);
            }

        }
    }

    public void notifierInfos(InfoTransfert infos, Client client) {
        Textes.printErr("Je gère une info !");

        /**
         * On gère selon le type de retour
         */
        if ("donneesEquipe".equals(infos.getType())) {
            Equipe equipe = Jeu.getSJ().getEquipe(client.getNumeroEquipe());
            Equipe equipeReseau = infos.getEquipe();

            // Recopie le nom
            equipe.setPays(equipeReseau.getPays());

            // Recopie la vue
            equipe.setVue(infos.getEquipe().getVue());

            for (Robot robot : equipeReseau.getRobots()) {
                equipe.ajoutRobot(robot);
            }
            client.setEstOperationnel(true);

        }

        // pour éviter de retransférer l'info
        infos.setClientOriginal(client);
        notifierInfos(infos);

    }

}
