/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualwar;

import virtualwar.plateau.Plateau;
import virtualwar.equipes.Equipe;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 *
 * @author youdelice
 */
public class PlateauTest {
    
    @Test
    public void test_pourcentageObstacle(){
        Equipe[] team = new Equipe[2];
        Equipe equipe = new Equipe("france");
        Equipe equipe2 = new Equipe("Allemagne");
       team[0]=equipe;
       team[1]=equipe2;
        int compt=0;
        for(int t=0;t<10000;t++){
        Plateau plateau = new Plateau(20, 20, team, 30);
        for(int x =0; x<10; x++){
           for(int y =0; y<10; y++){ 
               
               if((x!=0 && y!=0)||(x!=9 && y!=9)){
                   
               if(plateau.getCellule(x, y).contientObstacle()){
                   System.out.println("test");
                  compt++; 
               }}
               }
           }
        } 
        assertEquals(60, compt/10000);
    }
    
    @Test
    public void testnmbdebase(){
        Equipe[] team = new Equipe[2];
        Equipe equipe = new Equipe("france");
        Equipe equipe2 = new Equipe("Allemagne");
       team[0]=equipe;
       team[1]=equipe2;
        int compt=0;
        Plateau plateau = new Plateau(10, 10, team, 40);
        for(int x =0; x<10; x++){
           for(int y =0; y<10; y++){ 
               if(plateau.getCellule(x, y).estBase()){
                  compt++; 
               }
               }
           }
        assertTrue(plateau.getCellule(0, 0).estBase());
        assertTrue(plateau.getCellule(plateau.getHauteurPlateau()-1, plateau.getLargeurPlateau()-1).estBase());
    }

    @Test
    public void testGrandeurTerrain(){
         Equipe[] team = new Equipe[2];
        Equipe equipe = new Equipe("france");
        Equipe equipe2 = new Equipe("Allemagne");
       team[0]=equipe;
       team[1]=equipe2;
        int compt=0;
        Plateau plateau = new Plateau(10, 10, team, 40);
        assertEquals(plateau.getHauteurPlateau(), 10);
        assertEquals(plateau.getLargeurPlateau(), 10);
    }
    
        
}
