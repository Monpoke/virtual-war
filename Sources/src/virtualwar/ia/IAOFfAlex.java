package virtualwar.ia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import outils.Textes;
import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.general.Coordonnees;
import virtualwar.robots.Robot;
import virtualwar.robots.Tireur;

public class IAOFfAlex extends IA {
	private HashMap<Robot, Coordonnees> robotsPositions = new HashMap<>();
	private int rbt = 0;

	public IAOFfAlex(Equipe equipe) {
		super(equipe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void choisirRobots(int nbRobots) {
		  for(int i = 0; i < nbRobots; i++){
	            getEquipe().ajoutRobot(new Tireur(getEquipe()));
	        }
	}
	

	@Override
	public String jouer() {
		String retour = "";
		
		Robot robot = getEquipe().getRobots().get(this.rbt);
		this.rbt = (this.rbt + 1)%2;
		return retour;
		
		
	}
	
	 private void sauvegarderCoordonnees() {

	        // Equipes
	        for (Equipe equi : Jeu.getSJ().getEquipes()) {

	            // robots
	            for (Robot rob : equi.getRobots()) {

	                Coordonnees co = new Coordonnees(rob.getCoordonnees().getX(), rob.getCoordonnees().getY());

	                this.robotsPositions.put(rob, co);

	            }
	        }

	    }

	 /**
	     * Cette méthode permet d'analyser les robots sur le plateau
	     */
	    private HashMap<Robot, HashMap<Robot, Integer>> analyserPlateau() {

	        HashMap<Robot, HashMap<Robot, Integer>> robotsDistances = new HashMap<>();
	        List<Robot> mesRobots = new ArrayList<>();

	        // Get mes robots
	        for (Robot robot : this.getEquipe().getRobots()) {
	            robotsDistances.put(robot, new HashMap<>());
	            mesRobots.add(robot);
	        }

	        // foreach equipes
	        for (Equipe equi : Jeu.getSJ().getEquipes()) {
	            if (equi.equals(this.getEquipe())) {
	                continue;
	            }

	            for (Robot rob : equi.getRobots()) {
	                // calcul de la distance entre chaque robots
	                int x = rob.getCoordonnees().getX(),
	                        y = rob.getCoordonnees().getY();

	                Textes.printErr("Robot: " + rob.getIdentifiantReseau() + " " + x + "," + y);

	                // Est Sur Base donc on zappe
	                if (rob.estSurBase()) {
	                    continue;
	                }

	                // mettre la distance
	                for (Robot monRobot : mesRobots) {
	                    robotsDistances.get(monRobot).put(rob, this.nbCasesEntreRobots(monRobot.getCoordonnees().getX(), monRobot.getCoordonnees().getY(), x, y, monRobot));
	                }
	            }

	        }

	        return robotsDistances;

	    }
	    
	    private int nbCasesEntreRobots(int x1, int y1, int x2, int y2, Robot robotSeDeplacant) {
	        return (x1 - x2) + (y1 - y2);
	    }
}
