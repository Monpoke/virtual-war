package virtualwar.robots;

import virtualwar.equipes.Equipe;
import java.util.ArrayList;
import java.util.List;
import outils.Textes;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;

/**
 * Représente un piègeur.
 *
 * @author Florent
 */ 
public class Piegeur extends Robot {

    private int stockMines = 0;

    /**
     * Constructeur d'un piégeur
     *
     * @param equipe Equipe du piégeur
     */
    public Piegeur(Equipe equipe) {
        super(equipe);
        setEnergie(Constante.PIEGEUR.get("energie"));
        rechargerLesMines();
    }

    /**
     * Recharge le stock du piégeur avec le nombre maximum de mine
     *
     */
    public void rechargerMines() {
        if (estSurBase()) {
            rechargerLesMines();
        }
    }

    /**
     * Recharge les mines.
     */
    private void rechargerLesMines() {
        this.stockMines = Constante.PIEGEUR.get("nombreMineStock");
    }

    /**
     * Détermines si le robot peut poser des mines.
     * @return 
     */
    public boolean peutPoserMine() {
        return stockMines > 0;
    }

    /**
     * On lui retire une mine.
     */
    public void retirerMine() {
        stockMines--;
    }

    /**
     * Détermine si un piégeur peut miner.
     *
     * @return vrai si le piégeur peut agir.
     */
    @Override
    public boolean peutAgir() {
        return super.peutAgir() && peutPoserMine();
    }

    /**
     * Retourne le cout de l'action d'un piegeur
     *
     * @return le cout de l'action d'un piegeur
     */
    @Override
    public int getCoutAction() {
        return Constante.PIEGEUR.get("miner");
    }

    /**
     * Retourne le coût deplacement d'un piegeur
     *
     * @return le cout deplacement d'un piegeur
     */
    @Override
    public int getCoutDeplacement() {
        return Constante.PIEGEUR.get("deplacement");
    }

    /**
     * return le degat d'un tir d'un piegeur
     *
     * @return 0
     */
    @Override
    public int getDegatTir() {
        return 0;
    }

    /**
     * return le degat d'une mine d'un piegeur
     *
     * @return les degat d'une mine
     */
    @Override
    public int getDegatMine() {
        return Constante.DEGATS_MINE;
    }

    /**
     * return la porté d'un piegeur
     *
     * @return la porté d'un piegeur
     */
    @Override
    public int getPortee() {
        return Constante.PIEGEUR.get("portee");
    }

    /**
     * return le type d'un piegeur
     *
     * @return piegeur
     */
    @Override
    public String getType() {
        return "piegeur";
    }

    /**
     * return la liste des direction que le piegeur a pour se deplacer
     *
     * @return la liste des direction que le piegeur a pour se deplacer
     */
    @Override
    public List<Coordonnees> getDirectionsPossibles() {
        List<Coordonnees> listeCoordonnees = new ArrayList<>();

        listeCoordonnees.add(Constante.dep.get(0));     //  HAUT
        listeCoordonnees.add(Constante.dep.get(1));     //  HAUT_DROITE
        listeCoordonnees.add(Constante.dep.get(2));     //  DROITE
        listeCoordonnees.add(Constante.dep.get(3));     //  BAS_DROITE
        listeCoordonnees.add(Constante.dep.get(4));     //  BAS
        listeCoordonnees.add(Constante.dep.get(5));     //  BAS_GAUCHE
        listeCoordonnees.add(Constante.dep.get(6));     //  GAUCHE
        listeCoordonnees.add(Constante.dep.get(7));     //  HAUT_GAUCHE

        return listeCoordonnees;
    }

    /**
     * return la liste des direction que le piegeur a pour se deplacer
     *
     * @return la liste des direction que le piegeur a pour se deplacer
     */
    @Override
    public List<Coordonnees> getActionDirectionsPossibles() {
        List<Coordonnees> listeCoordonnees = new ArrayList<>();

        listeCoordonnees.add(Constante.dep.get(0));     //  HAUT
        listeCoordonnees.add(Constante.dep.get(1));     //  HAUT_DROITE
        listeCoordonnees.add(Constante.dep.get(2));     //  DROITE
        listeCoordonnees.add(Constante.dep.get(3));     //  BAS_DROITE
        listeCoordonnees.add(Constante.dep.get(4));     //  BAS
        listeCoordonnees.add(Constante.dep.get(5));     //  BAS_GAUCHE
        listeCoordonnees.add(Constante.dep.get(6));     //  GAUCHE
        listeCoordonnees.add(Constante.dep.get(7));     //  HAUT_GAUCHE

        return listeCoordonnees;
    }

    /**
     * Vérifie si il reste asser d'energie pour ce déplacer, et si il peut se
     * déplacer dans au moins une direction.
     *
     * @return true si le robot peut bouger, faut si aucun deplacement n'est
     * possible
     */
    @Override
    public boolean peutBouger() {
        return (getEnergie() - getCoutDeplacement()) > 0 && !getDirectionsPossibles().isEmpty();
    }

    /**
     * On recharge l'énergie du Piégeur.
     *
     * @param recupEnergie l'énergie à ajouter.
     */
    @Override
    public void rechargerEnergie(int recupEnergie) {
        super.rechargerEnergie(recupEnergie);
        if (this.getEnergie() > Constante.PIEGEUR.get("energie")) {
            this.setEnergie(Constante.PIEGEUR.get("energie"));
        }
    }

    /**
     * Fait un résumé de l'état pour le menu de droite.
     * 
     * @return 
     */
    @Override
    public String resumeEtat() {
        String r = super.resumeEtat();

        // On affiche le stock de mines si le piégeur n'est pas mort.
        if (!estMort()) {
            if (r.length() > 0) {
                r += " - ";
            }

            r += Textes.genererPluriel(stockMines, "Une mine", stockMines + " mines", "Stock de mine vide");
        }
        return r;
    }

    /**
     * Retire l'énergie d'une mine posée.
     */
    public void retirerMineEnergie() {

        this.setEnergie(getEnergie() - getCoutAction());

    }

}
