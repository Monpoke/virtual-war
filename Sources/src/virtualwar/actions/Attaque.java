package virtualwar.actions;

import outils.Sons;
import outils.Textes;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Case;
import virtualwar.plateau.Plateau;
import virtualwar.plateau.Mine;
import virtualwar.robots.Char;
import virtualwar.robots.Tireur;
import virtualwar.robots.Piegeur;
import virtualwar.robots.Robot;
import virtualwar.equipes.Vue;

/**
 * Cette classe représente les Attaques des robots.
 * 
 * @author Square N6
 */
public class Attaque extends Action {

    /**
     * Classe qui permet d'attaquer.
     *
     * @param robot le robot qui agit
     * @param direction direction du tir
     */
    public Attaque(Robot robot, Coordonnees direction) {
        super(robot, direction);

    }

    /**
     *
     * Permet de tirer ou de poser une mine, Attention il faut vérifier si
     * l'action est réalisable. De plus si lors du tir un robot allié se trouve
     * entre le robot tireur et le robot cible, ça sera lui qui sera touché.
     *
     */
    @Override
    public void agit() {
        // On vérifie si ce n'est pas une action réseau
        notifierServeur("attaque");
        
        Coordonnees Csto = getRobot().getCoordonnees();
        int x = Csto.getX() + getDirection().getX(); // coordonnées ou poser la mine
        int y = Csto.getY() + getDirection().getY();

        if (getRobot() instanceof Piegeur) { 
            piegerCase(x, y);
        } else if (getRobot() instanceof Char || getRobot() instanceof Tireur) {
            tirer(x, y);
        } else {
            Textes.printErr("Type de robot inconnu.");
        }
    }

    /**
     * Cette fonction est spécifique aux Pigeurs. Elle permet de piéger une case.
     * 
     * @param x coordonnée X d'une case
     * @param y coordonnée Y d'une case
     */
    private void piegerCase(int x, int y) {

        Vue vue = getRobot().getEquipe().getVue();

        if (vue.getPlateau().estDansPlateau(new Coordonnees(x, y))
                && vue.getPlateau().getCellule(x, y).estLibre()
                && !vue.getPlateau().getCellule(x, y).contientMine()
                && !(vue.getPlateau().getCellule(x, y).estBase())) {

            // On ajoute une mine.
            vue.getPlateau().getCellule(x, y).setMine(new Mine(getRobot().getEquipe()));
            ((Piegeur) getRobot()).retirerMine();
            
            Sons.jouer("miner");
            ((Piegeur) getRobot()).retirerMineEnergie();

        } // On teste si on veut poser une mine sur une autre
        else if (vue.getPlateau().estDansPlateau(new Coordonnees(x, y))
                && vue.getPlateau().getCellule(x, y).contientMine()) {

            vue.getPlateau().getCellule(x, y).videCase();

            ((Piegeur) getRobot()).retirerMineEnergie();
            ((Piegeur) getRobot()).retirerMine();

             Sons.jouer("mineExplose");
            
            Textes.printErr("Mine sur mine");
        } else {

            /*
             débugage
             */
            Textes.printErr("Minage impossible ici");
        }

    }

    /**
     * Permet de tirer sur une cible
     *
     * @param x coordonnée X d'une case
     * @param y cordonnées Y d'une case
     */
    private void tirer(int x, int y) {
        int i = 0;

        Plateau plateau = getRobot().getEquipe().getVue().getPlateau();

        /**
         *  On vérifie qu'il n'y a pas d'obstacle sur le chemin du robot
         */
        while (plateau.estDansPlateau(new Coordonnees((x), (y))) && // test que les coordonnées de la cellule sont toujours valides
                !plateau.getCellule(x, y).estBase() && // N'est pas une base
                !(plateau.getCellule(x, y).contientRobot()) && // vérifie si on trouve le robot
                !(plateau.getCellule(x, y).contientObstacle()) && //ou si on passe sur un obstacle
                (i <= this.getRobot().getPortee())) { // vérifie la portée

            x = x + getDirection().getX();
            y = y + getDirection().getY();
            i++;
        }
        
        
        // on a trouvé le robot sur lequel tirer, qui est en x y, ou un obstacle ou un outOfBounds.
        // on est obligé de cast pour utiliser la fonction getRobot
        if (plateau.estDansPlateau(new Coordonnees(x, y))
                && !(plateau.getCellule(x, y).contientObstacle())
                && plateau.getCellule(x, y).contientRobot()
                && !plateau.getCellule(x,y).estBase()) {

            Case caseSto = (Case) plateau.getCellule(x, y);

            if (caseSto.getRobot().getEquipe() != this.getRobot().getEquipe()) {
                caseSto.getRobot().setEnergie(caseSto.getRobot().getEnergie() - getRobot().getDegatTir());
                
                // le tir a été fait.
                if(getRobot() instanceof Char){
                    Sons.jouer("tirerChar");
                } else {
                    Sons.jouer("tirerTireur");
                }
            }
            
            
            
        } else {
            Textes.printErr("Erreur ! Le tir est impossible ici.");
        }

        // Le robot pert dans tout les cas l'énergie relative a son action
        getRobot().setEnergie(getRobot().getEnergie() - getRobot().getCoutAction());    
    }

}
