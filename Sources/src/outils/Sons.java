package outils;

import java.io.FileInputStream;
import java.io.InputStream;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import virtualwar.general.Constante;

/**
 * Cette classe représente une possibilité de jouer des sons.
 * 
 * @author Pierre
 */
public abstract class Sons {

    private static String pathFile;

    /**
     * Lance un son, passant par son identifiant.
     * @param nom
     * @return 
     */
    public static boolean jouer(String nom) {

        pathFile = Sons.trouverChemin(nom);

        if (pathFile.length() == 0) {
            return false;
        }

        try {
            // open the sound file as a Java input stream
            InputStream in = new FileInputStream(Sons.pathFile);
            // create an audiostream from the inputstream
            AudioStream audioStream = new AudioStream(in);
            // play the audio clip with the audioplayer class
            AudioPlayer.player.start(audioStream);

        } catch (Exception e) {
            Textes.printErr("Lecteur son: " + e.getMessage());
        }

        return true;
    }

    /**
     * Cette fonction sert à enregistrer chaque chemin des fichiers sons.
     *
     * @param nom
     * @return
     */
    private static String trouverChemin(String nom) {
        String chemin = "";
        if ("deplacement".equals(nom)) {
            chemin = "deplacement";
        } else if ("tirerTireur".equals(nom)) {
            chemin = "tirerTireur";
        } else if ("tirerChar".equals(nom)) {
            chemin = "tirerChar";
        } else if ("miner".equals(nom)) {
            chemin = "miner";
        } else if ("mineExplose".equals(nom)) {
            chemin = "mineExplose";
        }

        // On préfixe avec le chemin des ressources
        if (!chemin.isEmpty()) {
            chemin = Constante.RESOURCE_PATH + "\\songs\\" + chemin + ".wav";
        }

        return chemin;
    }

}
