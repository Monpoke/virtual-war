package reseau2.socket.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import outils.InfoTransfert;
import outils.Textes;

/**
 * Cette classe appartient à un client.
 *
 * @author Pierre
 */
public class ConnexionClient implements Runnable {

    private final Socket sockClient;

    ObjectInputStream in;
    ObjectOutputStream out;
    Thread receptOut;
    private Thread receptIn;

    /**
     * Prend en compte un socket client.
     * @param sockClient 
     */
    public ConnexionClient(Socket sockClient) {
        this.sockClient = sockClient;

    }

    @Override
    public void run() {

        try {
            out = new ObjectOutputStream(sockClient.getOutputStream());
            
            // ON ECOUTE LE SERVEUR
            in = new ObjectInputStream(sockClient.getInputStream());
            receptIn = new Thread(new EcouterServeur(this, in));
            receptIn.start();
            
        } catch (IOException e) {
            Textes.printErr(e.getMessage());
        }
        
    }

    /**
     * On envoie des informations au serveur.
     * @param info la classe InfoTransfert.
     */
    public void envoyer(InfoTransfert info) {
        try {
            out.writeObject(info);
            out.flush();
        } catch (IOException ex) {
            Textes.printErr(ex.getMessage());
        }
    }

    /**
     * On stoppe la connexion du client au serveur.
     */
    public void stopperConnexion() {
        try {
            out.close();
            sockClient.close();
            System.out.println("Socket ferme");
        } catch (IOException ex) {
            System.out.println("FERME " + ex.getMessage());
        }
    }

}
