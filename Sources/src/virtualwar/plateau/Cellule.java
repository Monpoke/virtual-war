package virtualwar.plateau;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import outils.Textes;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Mine;
import virtualwar.robots.Robot;
import virtualwar.equipes.Equipe;

import javax.imageio.ImageIO;

/**
 *
 * @author Square N6
 */
public abstract class Cellule implements Constante, Cloneable, Serializable {

    protected Equipe Base = null;

    protected Coordonnees coord;

    protected Mine mine = null;

    /**
     * classe cellule qui return les caract d'une cellule
     *
     * @param coord coord d'une cellule
     */
    public Cellule(Coordonnees coord) {
        this.coord = coord;
    }

    /**
     * return yes if this is Base, false if not
     *
     * @return this instanceof Base
     */
    public boolean estBase() {
        return this instanceof Base;
    }

    /**
     * Getter coord
     *
     * @return new Coordonnees
     */
    public Coordonnees getCoordonnees() {
        return new Coordonnees(this.coord.getX(), this.coord.getY());
    }

    /**
     *
     * @param mine
     */
    public void setMine(Mine mine) {
        this.mine = mine;
    }

    /**
     * return si la case a une mine
     *
     * @return si la case a une mine
     */
    public boolean contientMine() {
        return this.mine != null;
    }

    public Mine getMine() {
        return mine;

    }

    /**
     * methode qui affiche les caract de cellule
     *
     * @return les caract de cellule
     */
    @Override
    public String toString() {
        return " ";
    }

    /**
     *
     * vide la cellule
     */
    public abstract void videCase();

    /**
     * return si un robot peut se deplacer sur une cellule
     *
     * @param robot type de robot
     * @return vrai si un robot peut se deplace sur une cellule
     */
    public abstract boolean peutSeDeplacerSur(Robot robot);

    public abstract boolean contientObstacle();
    /*
     public abstract boolean contientMine();

     public abstract Mine getMine();
     public abstract void setMine(Mine mine);*/

    public abstract boolean estLibre();

    public abstract boolean contientRobot();

    public abstract void enleverRobot(Robot robot);

    public abstract void ajouterRobot(Robot robot);

    /**
     * Méthode pour cloner
     * @return 
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        Object o = null;
        try {
            // On récupère l'instance à renvoyer par l'appel de la 
            // méthode super.clone()
            o = super.clone();
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons 
            // l'interface Cloneable
            Textes.printErr(cnse.getMessage());
        }
        // on renvoie le clone
        return o;
    }

}
