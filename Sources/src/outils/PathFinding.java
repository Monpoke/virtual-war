package outils;

import virtualwar.plateau.Cellule;

import java.util.ArrayList;
import java.util.List;

/**
 * PathFinding regroupe des méthodes utilitaires en rapport avec le pathfinding.
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-04-02
 */
public abstract class PathFinding {

    /**
     * Cherche s'il existe un chemin entre depart et arrivee.
     *
     * @param plateau Plateau de jeu.
     * @param depart Coordonnée de départ.
     * @param arrivee Coordonnée d'arrivée.
     * @param diagonal Autorise les diagonales
     * @return True si il y a un chemin, false sinon.
     */
    public static boolean chemin(Cellule[][] plateau, Cellule depart, Cellule arrivee, boolean diagonal) {
        // Créé une liste pour stocker les cellules à visiter.
        List<Cellule> ouvertes = new ArrayList<>();
        // Ajoute la cellule de départ.
        ouvertes.add(depart);
        // Créé une liste pour stocker les cellules visitées.
        List<Cellule> visitees = new ArrayList<>();

        // Tant qu'il y a des cellules à visiter.
        while (ouvertes.size() > 0) {
            // On visite la prochaine cellule.
            Cellule suivante = ouvertes.get(0);

            // Si cette cellule est le point d'arrivée.
            if (suivante == arrivee) {
                // Réussite, il existe un chemin.
                return true;
            }

            // On retire cette cellule de la liste des cellules à visiter.
            ouvertes.remove(suivante);
            // On ajoute cette cellule à la liste des cellules visitées.
            visitees.add(suivante);

            // On créé une liste pour contenir toutes les cellules accessibles depuis celle-ci.
            List<Cellule> enfants = new ArrayList<>();

            // On regarde toutes les cellules adjacentes à celle-ci.
            for (int y = -1; y <= 1; y++) {
                for (int x = -1; x <= 1; x++) {
                    // Si x et y ne sont pas égal à 0 (pour ne pas traiter la cellule sur laquelle on est).
                    if (!(x == 0 && y == 0)) {
                        // Si on accepte les déplacements en diagonale, ou que x ou y est égal à 0 (pour ne pas traiter les diagonales).
                        if (diagonal || (x == 0 || y == 0)) {
                            // Try catch pour ne pas avoir à vérifier les bords du plateau de jeu.
                            try {
                                // On récupère la cellule adjacente.
                                Cellule enfant = plateau[suivante.getCoordonnees().getY() + y][suivante.getCoordonnees().getX() + x];
                                // Si la cellule ne contient pas d'obstacle.
                                if (!enfant.contientObstacle()) {
                                    // On ajoute la cellule à la liste des cellules accessibles.
                                    enfants.add(enfant);
                                }
                            } catch (ArrayIndexOutOfBoundsException horsDuPlateauDeJeu) {
                            }
                        }
                    }
                }
            }

            // Pour chaque cellule accessible.
            for (Cellule enfant : enfants) {
                // Si la cellule n'est pas déjà dans la liste des cellules à visiter, et qu'elle n'a pas déjà été visitée.
                if (!(ouvertes.contains(enfant) || visitees.contains(enfant))) {
                    // On ajoute la cellule à la liste des cellules à visiter.
                    ouvertes.add(enfant);
                }
            }
        }
        // Si on a épuisé la liste des cellules à visiter, et qu'on a pas atteint le point d'arrivée, il n'existe pas de chemin.
        return false;
    }

}
