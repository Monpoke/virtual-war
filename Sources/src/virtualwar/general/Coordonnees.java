package virtualwar.general;

import java.io.Serializable;

/**
 * Classe utilitaire pour représenter des coordonnées.
 *
 * @author Square N6
 */
public class Coordonnees implements Serializable {

    private int x;

    private int y;

    /**
     * classe des coordonnées d'un cellule
     *
     * @param x l'abcisse de la cellule
     * @param y l'ordonné de la cellule
     */
    public Coordonnees(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Retourne l'abcisse de la cellule
     *
     * @return l'abcisse de la cellule
     */
    public int getX() {
        return x;
    }

    /**
     * Retourne l'ordonnée de la cellule
     *
     * @return l'ordonnée de la cellule
     */
    public int getY() {
        return y;
    }

    /**
     * Méethode qui retourne les coordonnée d'une celulle
     *
     * @return les coordonnées d'une cellule
     */
    @Override
    public String toString() {
        return x + "|" + y;
    }

    /**
     * return les coordonnees du vecteur
     *
     * @param coord coordonnes d'une cellule
     * @return les coordonnees du vecteur
     */
    public Coordonnees ajoutVecteur(Coordonnees coord) {
        return new Coordonnees(coord.getX() + this.x, coord.getY() + this.y);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.x;
        hash = 47 * hash + this.y;
        return hash;
    }

    public boolean equals(Coordonnees coords) {
        return coords != null && coords.getX() == x && coords.getY() == y;
    }

    public boolean enDessousDe(Coordonnees c) {
        return (this.y - c.getY()) > 0;
    }

    public boolean aGaucheDe(Coordonnees c) {
        return (this.x - c.getX()) < 0;
    }

    /**
     * Retourne vrai si un vecteur est colinéaire
     *
     * @param c
     * @return
     */
    public boolean estColineaireA(Coordonnees c) {
        return (this.x * c.y) == (this.y * c.x);
    }

    
    /**
     * Même direction ?
     * @param c
     * @return 
     */
    public boolean memeDirectionQue(Coordonnees c) {
        Coordonnees multi = new Coordonnees(c.getX() * this.getX(), c.getY() * this.getY());

        return (multi.getX() >= 0 && multi.getY() >= 0);

    }

}
