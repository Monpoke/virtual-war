package virtualwar.equipes;

import java.io.Serializable;
import outils.Textes;
import virtualwar.actions.Action;
import virtualwar.fenetres.Jeu;
import virtualwar.general.Constante;
import virtualwar.plateau.Base;
import virtualwar.plateau.Case;
import virtualwar.plateau.Cellule;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Plateau;
import virtualwar.robots.Robot;

/**
 * Cette classe représente la vue d'une équipe.
 * 
 * @author Square N6
 */
public class Vue implements Serializable {


	Plateau plateau;
	Equipe equipe;

	/**
	 * Constructeur de vue
	 *
	 * @param plateau plateau de jeu
	 * @param equipe la vue de l'equipe
	 *
	 */
	public Vue(Plateau plateau, Equipe equipe) {
		this.plateau = plateau;
		this.equipe = equipe;
	}

	/**
	 * Retourne le plateau
	 *
	 * @return plateau
	 */
	public Plateau getPlateau() {
		return plateau;
	}

	/**
	 * Cette fonction retourne un tableau de cellules filtrées. Aucune opération
	 * ne doit être faite sur ces cellules !
	 *
	 * @return les cellules filtrées pour les vues surtout.
	 */
	public Cellule[][] getPlateauFiltre() {
		Cellule[][] plateau = getPlateau().getPlateau();

		Cellule[][] cellulesFiltrees = new Cellule[getPlateau().getHauteurPlateau()][getPlateau().getLargeurPlateau()];

		// Clonage des cellules
		for (int y = 0; y < getPlateau().getHauteurPlateau(); y++) {
			for (int x = 0; x < getPlateau().getLargeurPlateau(); x++) {
				Cellule cellPlateau = plateau[y][x];

				try {
					Cellule filtree = (Cellule) (cellPlateau.clone());

					// On efface les mines qui n'appartiennent pas à l'équipe.
					if(filtree.contientMine() && !filtree.getMine().getEquipe().equals(this.equipe)){
						filtree.setMine(null);
					}

					cellulesFiltrees[y][x] = filtree;
				} catch (Exception e) {
					Textes.printErr(e.getMessage());
				}

			}
		}

		return cellulesFiltrees;
	}

	/**
	 * Vérifie si des coordonnées sont dans le plateau.
	 * 
	 * @param coord
	 * @return 
	 */
	public boolean coordDansPlateau(Coordonnees coord) {
		if (coord.getX() >= 0 && coord.getY() >= 0 && coord.getX() < plateau.getLargeurPlateau() && coord.getY() < plateau.getHauteurPlateau()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Vérifie si un déplacement est ok.
	 * 
	 * @param robot Désigne le robot qui se déplace
	 * @param coord Désigne les coordonnées de la case à aller.
	 * @return
	 */
	public boolean deplacementOK(Robot robot, Coordonnees coord) {

		// Si les coordonnées ne sont pas dans le plateau.
		if (!coordDansPlateau(coord)) {
			return false;
		}

		// On récupère la cellule
		Cellule cell = getPlateau().getCellule(coord);

		Textes.printErr("Je teste mon deplacement !");

		if (cell.estBase() && robot.getEquipe().getBase() != (Base) cell) {
			return false;
		} else if (cell.estBase()) {

			return (robot.getEquipe().getNbRobotsOperationnels()-robot.getEquipe().getBase().compterRobotsVivants()>1);
		} else {
			return ((Case) cell).estLibre();
		}
	} 

	public boolean tireOK(Robot robot, Coordonnees testDirection) {
		
		return false;
	}
        
    /**
     * Renvoie une direction valide si elle existe pour que le robot 
     * se rapproche directement ( donc pas programmé pour faire le tour de l'obstacle) 
     * du robot enemie le plus proche. Fonction non testée
     * @param robot
     * @return une direction (coordonée) vers le robot ennemie le plus proche 
     * ou null si il n'y à pas de direction valide)
     */
    public Coordonnees robotEnnemiePlusProche(Robot robot){
            // pour chaque équipe ennemie
            int min=1000; // valeur minimale de distance des robots (1000 valeurs maximale possible)
            Coordonnees valReturn=null;
        for (Equipe equipeEnnemie : Jeu.getSJ().getEquipes()){
            if(equipeEnnemie!=this.equipe){
                for (Robot robotEnnemie : equipeEnnemie.getRobots()) {
                    if(distanceEntreRobot(robotEnnemie, robot)<min && peutSeDeplacerVers(robot,robotEnnemie)!= null){
                        min=distanceEntreRobot(robotEnnemie, robot);
                        valReturn=peutSeDeplacerVers(robot, robotEnnemie); // bug possible ici car non instancié.   
                        if(Constante.DEBUG_MODE){
                            System.out.println("robot le plus près trouvée dans la direction de "+valReturn);
                        }
                    }
                }
            }
        }
            return valReturn;
    }
        
    /**
     * Renvoie la distance en nombre de case entre 2 si il ne eput pas se déplacer en diagonale. 
     * Fonction non testée
     * @param robot1
     * @param robot2
     * @return la distance en nombre de case entre 2 robot.
     */
    public int distanceEntreRobot(Robot robot1,Robot robot2){
    int valReturn; // valeur de la distance via la formule trouvée sur wikipedia
        valReturn = (int) Math.sqrt(Math.pow(robot2.getCoordonnees().getX()-robot1.getCoordonnees().getX(),2)
                +(Math.pow(robot2.getCoordonnees().getY()-robot1.getCoordonnees().getY(),2)));
            
            if(Constante.DEBUG_MODE){
                System.out.println("distance entre"+robot1+" et "+robot2+" est de"+valReturn);
            }
            return valReturn;
            
            
        }

    /**
     * La direction dans laquelle le robot1 doit aller pour se rapprocher directement du robot2 
     * ou null si  il est bloqué par des obtacle.
     * Fonction non testée
     * Attention non codé pour les tireur qui eux peuve se déplacer en diagonale.
     * @param robot1
     * @param robot2
     * @return une direction( Coordonée) ou null si il n'y a pas de direction possible
     */
    public  Coordonnees peutSeDeplacerVers(Robot robot1, Robot robot2){
        
        // test si le robot1 est en bas ou en heut du robot2
        if(robot1.getCoordonnees().getX()<robot2.getCoordonnees().getX()){
            
            if(this.deplacementOK(robot1, robot1.getCoordonnees().ajoutVecteur(Constante.DROITE))){
                return Constante.DROITE;
            }
        }
         // attention ici else non possible car il peuvent être sur la même ligne.
        if(robot1.getCoordonnees().getX()>robot2.getCoordonnees().getX()){
           
            if(this.deplacementOK(robot1, robot1.getCoordonnees().ajoutVecteur(Constante.GAUCHE))){
                return Constante.GAUCHE;
            }
        }
        
        if(robot1.getCoordonnees().getY()>robot2.getCoordonnees().getY()){
           
            if(this.deplacementOK(robot1, robot1.getCoordonnees().ajoutVecteur(Constante.HAUT))){
                return Constante.HAUT;
            }
        } 
        
                if(robot1.getCoordonnees().getY()<robot2.getCoordonnees().getY()){
                 
            if(this.deplacementOK(robot1, robot1.getCoordonnees().ajoutVecteur(Constante.BAS))){

                return Constante.BAS;
            }
        }
    
    if(Constante.DEBUG_MODE){
        System.out.println("Pas de direction possible pour "+robot1);
    }
    return null; // il n'y a pas de direction possible.
    
    
    }
}
