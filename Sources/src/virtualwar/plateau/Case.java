package virtualwar.plateau;

import outils.Textes;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Obstacle;
import virtualwar.robots.Robot;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Florent
 */
public class Case extends Cellule {

    private Robot robot = null;

    protected Obstacle obstacle = null;

    /**
     * class mere de cellule
     *
     * @param coord coordonees de la case
     */
    public Case(Coordonnees coord) {
        super(coord);
    }

    /**
     * return si un robot est sur la case
     *
     * @return si un robot est present
     */
    public Robot getRobot() {
        return this.robot;
    }

    public Obstacle getCase() {
        return obstacle;
    }

    /**
     * return si la case est un obstacle
     *
     * @return si la case est un obstacle
     */
    public Obstacle getObstacle() {
        return this.obstacle;
    }

    public void setObstacle(Obstacle obstacle) {
        this.obstacle = obstacle;
    }

    /**
     * Retourne vrai si il y a un robot sur la case.
     *
     * @return
     */
    @Override
    public boolean contientRobot() {
        return this.getRobot() != null;
    }

    /**
     * Vide la case.
     */
    @Override
    public void videCase() {
        this.obstacle = null;
        this.robot = null;
        this.mine = null;
    }

    /**
     * Retourne vrai si un robot peut se déplacer.
     *
     * @param robot type de robot
     * @return vrai si un robot peut se deplacer sur la case
     */
    @Override
    public boolean peutSeDeplacerSur(Robot robot) {
        return obstacle == null && getRobot() == null; // ici on considère que le robot est à distance

    }

    /**
     * Retourne vrai si la case contient un obstacle.
     *
     * @return
     */
    @Override
    public boolean contientObstacle() {
        return this.obstacle != null;
    }

    /**
     * Retourne vrai si la case est libre.
     *
     * @return vrai si la case est libre.
     */
    @Override
    public boolean estLibre() {
        return !(contientObstacle() || contientRobot());
    }

    /**
     * Enlève un robot de la case.
     *
     * @param robot
     */
    @Override
    public void enleverRobot(Robot robot) {
        if (getRobot() == robot) {
            this.robot = null;
        }
    }

    @Override
    public void ajouterRobot(Robot robot) {
        this.robot = robot;
        System.out.println("ROBOT AJOUTE Case 119");
    }

    @Override
    public String toString() {

        if (contientObstacle()) {
            return "O";
        } else if (contientMine()) {
            return "X";
        }
        if (this.robot != null) {
            Textes.printErr("CONTIENT ROB?");
        }
        return (contientRobot()) ? getRobot().toString() : " ";
    }

}
