package virtualwar.general;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Représente les différentes constantes du jeu.
 *
 * @author Square N6
 */
public interface Constante {

    /**
     * Random generator of the game.
     */
    Random rand = new Random();

    String RESOURCE_PATH = "assets\\";

    // Utile pour activer les paramètres de débuggage ou non.

    boolean DEBUG_MODE = true;       // les ia ne sont pas encore proposées à false
    boolean debug_IA=false;
    
    int PAYS_ROBOTS_MAX = 5;
    int PAYS_ROBOTS_MIN = 2;

    int TERRAIN_LARGEUR_MAX = 30;
    int TERRAIN_LARGEUR_MIN = 10;

    int TERRAIN_HAUTEUR_MAX = 30;
    int TERRAIN_HAUTEUR_MIN = 10;

    int TERRAIN_PERCENTAGE_OBJETS_MAX = 75;

    int DEGATS_MINE = 2;

    int BASE_RECUPERATION_ENERGIE = 4;

    final int MARGE_POURCENTAGE_OBSTACLE = 10;
    
    // RESEAU
    int RESEAU_PORT = 1996;
    String IP_PATTERN = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";


    /**
     * Spécificités des robot tireurs.
     */
    HashMap<String, Integer> TIREUR = new HashMap<String, Integer>() {
        {   /*
             Portée du tir, en nombre case
             */

            put("portee", 3);
            /*
             nombre de case ou il peut se déplacer
             */
            put("deplacement", 1);
            /*
             Energie de départ
             */
            put("energie", 40);
            /*
             Régeneration de l'ernergie dans la base par tour de l'équipe allié
             */
            put("base", 2);
            /*
             cout en énergie effectuer pour le tir
             */
            put("tirer", 2);
            /*
             cout en énergie pour effectuer le déplacement 
             */
            put("avancer", 1);
            /* 
             dégat que inflige le tir sur le robot cible
             */
            put("degat", 3);

        }
    };

    /**
     * Spécificités des robot piégeurs.
     */
    HashMap<String, Integer> PIEGEUR = new HashMap<String, Integer>() {
        {   /*
             Portée du posage de mine, en nombre case
             */

            put("portee", 1);
            /*
             nombre de case effectué lors du déplacement
             */
            put("deplacement", 1);
            /*
             Energie de départ
             */
            put("energie", 50);
            /*
             Régeneration de l'ernergie dans la base par tour de l'équipe allié
             */
            put("base", 2);
            /*
             cout en énergie effectuer pour le minage
             */
            put("miner", 2);
            /*
             cout en énergie pour effectuer le déplacement 
             */
            put("avancer", 2);
            /*
             Nombre de mine en stock 
             */
            put("nombreMineStock", 10);
            /*
             nombre d'énergie infligée quand un robot allié ou ennemie passe sur la mine;
             */

            put("Degat de la mine", 2);
        }
    };

    /**
     * Spécificités des robot char.
     */
    HashMap<String, Integer> CHAR = new HashMap<String, Integer>() {
        {   /*
             Portée du tir, en nombre case
             */

            put("portee", 10);
            /*
             nombre de case effectué lors du déplacement
             */
            put("deplacement", 2);

            /*
             Energie de départ
             */
            put("energie", 60);
            /*
             Régeneration de l'ernergie dans la base par tour de l'équipe allié
             */
            put("base", 2);
            /*
             cout en énergie pour effectuer le déplacement 
             */
            put("tirer", 1);
            /*
             cout en énergie pour effectuer le déplacement 
             */
            put("avancer", 5);
            /* 
             dégat que inflige le tir sur le robot cible
             */
            put("degat", 6);

        }
    };

    public final static Coordonnees HAUT = new Coordonnees(0, -1);
    public final static Coordonnees HAUT_DROITE = new Coordonnees(1, -1);
    public final static Coordonnees DROITE = new Coordonnees(1, 0);
    public final static Coordonnees BAS_DROITE = new Coordonnees(1, 1);
    public final static Coordonnees BAS = new Coordonnees(0, 1);
    public final static Coordonnees BAS_GAUCHE = new Coordonnees(-1, 1);
    public final static Coordonnees GAUCHE = new Coordonnees(-1, 0);
    public final static Coordonnees HAUT_GAUCHE = new Coordonnees(-1, -1);
    public final static Coordonnees CHAR_HAUT = new Coordonnees(0, -2);
    public final static Coordonnees CHAR_DROITE = new Coordonnees(2, 0);
    public final static Coordonnees CHAR_BAS = new Coordonnees(0, 2);
    public final static Coordonnees CHAR_GAUCHE = new Coordonnees(-2, 0);

    /**
     * Représente la liste des différentes coordonnées
     */
    public static List<Coordonnees> dep = new ArrayList<Coordonnees>() {
        {
            add(0, HAUT);  // 1
            add(1, HAUT_DROITE);
            add(2, DROITE);
            add(3, BAS_DROITE);
            add(4, BAS);   // 5
            add(5, BAS_GAUCHE);
            add(6, GAUCHE);
            add(7, HAUT_GAUCHE);
            add(8, CHAR_HAUT);
            add(9, CHAR_DROITE);
            add(10, CHAR_BAS);
            add(11, CHAR_GAUCHE);
        }
    };
}
