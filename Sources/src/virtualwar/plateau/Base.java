package virtualwar.plateau;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import virtualwar.plateau.Mine;
import virtualwar.robots.Robot;
import virtualwar.equipes.Equipe;
import java.util.ArrayList;
import java.util.List;
import virtualwar.general.Coordonnees;

import javax.imageio.ImageIO;

/**
 *
 * @author Florent Marcaille, Pierre Bourgeois
 */
public class Base extends Cellule implements Serializable {

    /**
     * Robots présents dans la base.
     */
    private List<Robot> robots;

    /**
     * Équipe possédant la base.
     */
    private Equipe equipe;

    /**
     * Crée une nouvelle base.
     *
     * @param coord Coordonnée de la base.
     * @param equipe Équipe à qui appartient la base.
     */
    public Base(Coordonnees coord, Equipe equipe) {
        super(coord);

        this.equipe = equipe;
        this.robots = new ArrayList<>();
    }

    /**
     * Getter retournant l'équipe de la base
     *
     * @return l'équipe de la base
     */
    public Equipe getEquipe() {
        return this.equipe;
    }

    /**
     * retourne la liste des robots dans la base
     *
     * @return la liste des robot dans la liste
     */
    public List<Robot> getRobots() {
        return robots;
    }

    /**
     * Retourne si la base a une mine
     *
     * @return toujours faux
     */
    @Override
    public boolean contientMine() {
        return false;
    }

    /**
     * Méthode vidant la liste de robots contenu dans la base et met l'équipe à
     * null
     */
    @Override
    public void videCase() {
        robots.clear();
        equipe = null;
    }

    /**
     * Retourne si un robot peut se deplacer sur la base
     *
     * @param robot le type de robot
     * @return vrai si il peut se deplacer dans la base
     */
    @Override
    public boolean peutSeDeplacerSur(Robot robot) {
        if (!robot.estSurBase() && robot.getEquipe() == this.equipe) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * La base ne peut contenir d'obstacle
     *
     * @return null
     */
    @Override
    public boolean contientObstacle() {
        return false;
    }

    /**
     * La base est toujours libre
     *
     * @return true
     */
    @Override
    public boolean estLibre() {
        return true;
    }

    /**
     * Renvoi le caractere qui defini une base
     *
     * @return "b"
     */
    @Override
    public String toString() {
        return (equipe.getNumero() == 0) ? "b" : (equipe.getNumero() == 1) ? "B" : (equipe.getNumero() == 2) ? "ⓑ" : "Ⓑ";
    }

    @Override
    public void setMine(Mine mine) {

    }

    @Override
    public boolean contientRobot() {
        return robots.size() != 0;
    }

    @Override
    public Mine getMine() {
        return null;

    }

    @Override
    public void enleverRobot(Robot robot) {
        this.robots.remove(robot);
    }

    @Override
    public void ajouterRobot(Robot robot) {
        this.robots.add(robot);
    }

    /**
     * Retourne le nombre de robots opérationnels.
     *
     * @return
     */
    public int compterRobotsVivants() {
        int nb = 0;
        for (Robot robot : robots) {
            if (!robot.estMort()) {
                nb++;
            }
        }
        return nb;

    }

}
