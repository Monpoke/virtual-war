package virtualwar.plateau;

import virtualwar.general.Constante;

import java.awt.*;
import java.io.Serializable;
 
/**
 * Obstacle représente un objet sur le plateau de jeu que les robots et missiles
 * ne peuvent pas franchir.
 *
 * @author Quentin Brault, Pierre Bourgeois
 */
public class Obstacle implements Constante, Serializable {

    /**
     * Type de l'obstacle
     */
    private String type;

    /**
     * Retourne le type de l'obstacle.
     *
     * @return Type de l'obstacle.
     */
    public String getType() {
        return "obstacle";
    }

    /**
     * Méthode qui permet l'affichage d'un obstacle.
     * @return 
     */
    public String toString() {
        return "O";
    }

}
