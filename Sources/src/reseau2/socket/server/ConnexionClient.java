package reseau2.socket.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import outils.Textes;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;

/**
 * Représente la connexion d'un client.
 * @author Pierre
 */
class ConnexionClient implements Runnable {

    private final ServerSocket ss;
    private Socket socket;
    private Thread t1;
    private final Serveur serveur;

    public ConnexionClient(ServerSocket ss, Serveur serveur) {
        this.ss = ss;
        this.serveur = serveur;
    }

    @Override
    public void run() {

        try {
            /**
             * On teste avec un seul client pour le moment
             */
            while(getServeur().getNombreConnectes() < Jeu.getSJ().getNbEquipeDistantes()){
                
                socket = ss.accept();
                Textes.printErr("Un client est connecté.");

                
                int numeroEquipe = trouverNumeroEquipe();
                Client client = new Client(ss, socket, this, numeroEquipe);
                
                t1 = new Thread(client);
                t1.start();
                
                /**
                 * On doit informer le client des plateaux de jeu
                 */
                client.envoyerDonneesPartie();
                
                /**
                 * On l'enregistre dans le serveur.
                 */
                getServeur().nouveauClient(socket, client);
                
            }
        } catch (IOException e) {

            System.err.println("Erreur serveur");
        }

    }

    public Serveur getServeur() {
        return serveur;
    }


    private int trouverNumeroEquipe() {
        Equipe[] equipes = Jeu.getSJ().getEquipes();
        int n = 0;
        
        for (Equipe equipe : equipes) {
            if(equipe.isJoueurReseau() && equipe.getIdJoueurReseau() == null){
                equipe.setIdJoueurReseau(socket);
                n = equipe.getNumero();
                break;
            }
        }        
        return n;
    }
    
    
}
