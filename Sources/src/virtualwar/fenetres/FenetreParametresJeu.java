/*
 *  Pierre Bourgeois - 2014
 */
package virtualwar.fenetres;

import java.util.ArrayList;
import java.util.List;
import outils.InfoTransfert;
import outils.Textes;
import reseau2.socket.server.Serveur;
import virtualwar.ia.Offensive;
import virtualwar.plateau.Base;
import virtualwar.robots.Char;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.equipes.Equipe;
import virtualwar.robots.Piegeur;
import virtualwar.plateau.Plateau;
import virtualwar.robots.Robot;
import virtualwar.robots.Tireur;
import virtualwar.equipes.Vue;
import virtualwar.ia.Aleatoire;
import virtualwar.ia.IAIntelligente;

/**
 * Demande les paramètres pour la partie en cours.
 *
 * @author Pierre Bourgeois
 */
public class FenetreParametresJeu extends Fenetre {

    /**
     * Détermine si le jeu est en réseau.
     */
    boolean networkGame = false;
    private InfoTransfert networkInformations = null;

    public FenetreParametresJeu() {
        super("Fenêtre principale");

    }

    /**
     * Code principale de la fenêtre.
     */
    @Override
    public void lancer() {

        if (networkGame == false || networkInformations == null) {
            demanderTousLesParametres();
        } else {
            completerPartieInformation();
        }

    }

    /**
     * Permet de lancer
     */
    private void demanderTousLesParametres() {

        /**
         * Demande les équipes
         */
        demanderEquipes();

        /**
         * Paramètres de la partie
         */
        demanderParametres();

        /**
         * Gère les robots par equipe
         */
        for (Equipe equipe : Jeu.getSJ().getEquipes()) {
            if (!equipe.isJoueurReseau()) {
                constituerEquipe(equipe);
            }
        }

        /**
         * On lance le serveur si c'est une partie réseau.
         */
        lancerServeurReseau();
        
        /**
         * Enfin, on lance le jeu !
         */
        Jeu.lancerJeu();
    }

    /**
     * Demande les paramètres du plateau.
     */
    private void demanderParametres() {
        if(Constante.debug_IA==true){
            creerPlateau(10, 10, 0);
            return;
        }
        setNom("Paramètres du plateau ?");
        afficheFenetre();

        int hauteur = 0, largeur = 0, pourcentageObjets = 0, nombreRobots = 0;
        String input = "";

        /**
         * On demande la hauteur
         */
        do {
            if (input.length() > 0) {
                afficherErreur("La hauteur demandée est invalide !");
            }

            System.out.println("Quelle est la hauteur du plateau ? (" + Constante.TERRAIN_HAUTEUR_MIN + "-" + Constante.TERRAIN_HAUTEUR_MAX + ")");
            input = scan.next();
            hauteur = convertStringToInt(input, -1);
        } while (!(hauteur >= Constante.TERRAIN_HAUTEUR_MIN && hauteur <= Constante.TERRAIN_HAUTEUR_MAX));

        /**
         * On demande la largeur
         */
        input = "";
        do {
            if (input.length() > 0) {
                afficherErreur("La largeur demandée est invalide !");
            }

            System.out.println("Quelle est la largeur du plateau ? (" + Constante.TERRAIN_LARGEUR_MIN + "-" + Constante.TERRAIN_LARGEUR_MAX + ")");
            input = scan.next();
            largeur = convertStringToInt(input, -1);
        } while (!(largeur >= Constante.TERRAIN_HAUTEUR_MIN && largeur <= Constante.TERRAIN_HAUTEUR_MAX));

        /**
         * On demande le pourcentage d'obstacles
         */
        input = "";
        do {
            if (input.length() > 0) {
                afficherErreur("Le pourcentage demandé est invalide !");
            }
            System.out.println("Quel est le pourcentage d'obstacles à générer ? (" + 0 + "-" + Constante.TERRAIN_PERCENTAGE_OBJETS_MAX + ")");
            input = scan.next();
            pourcentageObjets = convertStringToInt(input, -1);
        } while (!(pourcentageObjets >= 0 && pourcentageObjets <= Constante.TERRAIN_PERCENTAGE_OBJETS_MAX));

        // On crée le plateau maintenant
        creerPlateau(largeur, hauteur, pourcentageObjets);
    }

    /**
     * On crée le plateau.
     *
     * @param largeur
     * @param hauteur
     * @param pourcentageObstacles
     */
    private void creerPlateau(int largeur, int hauteur, int pourcentageObstacles) {
        Plateau plateau = new Plateau(largeur, hauteur, Jeu.getSJ().getEquipes(), pourcentageObstacles);
        Jeu.getSJ().setPlateau(plateau);
    }

    /**
     * Demande les équipes
     */
    private void demanderEquipes() {
        if(Constante.debug_IA==true){
            Equipe[] equipes = creerEquipes(2, 1, 0);
             Jeu.getSJ().setEquipes(equipes);
             Jeu.getSJ().setNbRobotsEquipes(2);
            return;
        }
        setNom("Pays qui vont s'affronter");
        afficheFenetre();

        int nbEquipes = demanderNombre("Combien d'équipe souhaitez-vous ?", 2, 4, "Entrez un nombre d'équipe correct !");

        /**
         * Si c'est une partie réseau, combien de postes distants ?
         */
        int nombreReseau = demanderNombreEquipeReseau(nbEquipes);

        // Nombre d'IA
        int nbIA = 0;
        // Pour le moment, on n'active les IA qu'en mode debug
        if (Constante.DEBUG_MODE && (nbEquipes - nombreReseau -1 ) > 0) {
            nbIA = demanderNombre("Combien d'IA ?", 0, (nbEquipes - nombreReseau - 1), "Entrez un nombre d'IA correct !");
        } 

        /**
         * On enregistre les équipes
         */
        Equipe[] equipes = creerEquipes(nbEquipes, nbIA, nombreReseau);
        Jeu.getSJ().setEquipes(equipes);

        /**
         * On demande le nombre de robots par équipe
         */
        int nbRobots = demanderNombre("Nombre de robots par pays ?", Constante.PAYS_ROBOTS_MIN, Constante.PAYS_ROBOTS_MAX, "Entrez un nombre correct de robots.");

        // On définit le nombre d'équipe
        Jeu.getSJ().setNbRobotsEquipes(nbRobots);

    }

    /**
     * Crée les équipes.
     *
     * @param nbEquipes
     * @param scan
     * @return
     */
    private Equipe[] creerEquipes(int nbEquipes, int nbIA, int nbReseau) {
        Equipe[] equipes = new Equipe[nbEquipes];

        // liste des noms pays
        List<String> lesPays = new ArrayList<>();

        int nbEquipesReelles = nbEquipes - nbIA - nbReseau;
        int nbEquipesReseau = nbReseau;

        // On crée les équipes
        for (int i = 0; i < nbEquipes; i++) {
            String nomPays = "";

            Equipe e;

            /**
             * Gère les équipes réelles, non gérées par les IA.
             */
            if (nbEquipesReelles > 0) {

                // on récupère un nom de pays avec caractères >= 3
                nomPays = demanderNom(lesPays, i);

                // on enregistre les noms d'équipe temporairement
                lesPays.add(nomPays.toLowerCase());

                // On crée une nouvelle équipe
                e = creerEquipe(nomPays);
                nbEquipesReelles--;
            } /**
             * On crée les équipes réseau maintenant.
             */
            else if (nbEquipesReseau > 0) {
                e = creerEquipe("Externe n°" + i);
                e.setIsJoueurReseau(true);
                nbEquipesReseau--;
            } else {
                e = creerEquipe("IA n°" + i);
                
                /**
                 * @TODO: On choisira une IA Offensive ici, 
                 * Possibilité de faire une fenètre pour chosir l'IA quand elles seront opérationels
                 */
                //e.setIa(new Aleatoire(e));
                e.setIa(new Offensive(e));
                //e.setIa(new IAIntelligente(e));
            }

            equipes[i] = e;
        }

        // Enregistre les équipes
        Jeu.getSJ().setEquipes(equipes);

        return equipes;
    }

    /**
     * Constitution des différentes équipes.
     *
     * @param equi
     */
    private void constituerEquipe(Equipe equi) {
        effacerEcran();

        // on Crée une vue
        equi.setVue(new Vue(Jeu.getSJ().getPlateau(), equi));

        // On appelle la méthode pour constituer la composition de l'équipe pour les IA
        if (equi.isIA()) {
            equi.getIa().choisirRobots(Jeu.getSJ().getNbRobotsEquipes());
        } else {
            if(Constante.debug_IA){
                while (equi.getRobots().size() < Jeu.getSJ().getNbRobotsEquipes()){
                    equi.ajoutRobot(new Char(equi));
                    
                }
                return;
            }
            /**
             * On va afficher le plateau
             */
            System.out.println(Jeu.getSJ().getPlateau());
            System.out.println("Constitution de l'armée du pays `" + equi.getPays() + "`");

            do {
                int restants = Jeu.getSJ().getNbRobotsEquipes() - equi.getRobots().size();
                System.out.println("\tVous avez encore le droit à " + Textes.genererPluriel(restants, "un robot", restants + " robots", "aucun robot") + ".");

                // On compte le nombre de robot
                int nbChars = equi.compterTypeRobot("char"),
                        nbTireurs = equi.compterTypeRobot("tireur"),
                        nbPiegeurs = equi.compterTypeRobot("piegeur");

                System.out.println("\t1) Un char (" + nbChars + ")");
                System.out.println("\t2) Un tireur (" + nbTireurs + ")");
                System.out.println("\t3) Un piégeur (" + nbPiegeurs + ")");

                String input = "";
                int choixChar;
                do {
                    if (input.length() > 0) {
                        afficherErreur("Le robot demandé n'existe pas.");
                    }

                    System.out.print("Choisissez un robot à ajouter : ");
                    input = scan.next();
                    choixChar = convertStringToInt(input, -1);
                } while (!(choixChar >= 1 && choixChar <= 3));

                /**
                 * Gestion du char à ajouter
                 */
                Robot robot;
                switch (choixChar) {
                    case 1:
                        robot = new Char(equi);
                        break;
                    case 2:
                        robot = new Tireur(equi);
                        break;
                    case 3:
                    default:
                        robot = new Piegeur(equi);
                        break;
                }

                // On ajoute le robot maintenant
                equi.ajoutRobot(robot);

            } while (equi.getRobots().size() < Jeu.getSJ().getNbRobotsEquipes());

        }
    }

    /**
     * Cette méthode crée une équipe à partir d'un nom.
     *
     * @param nomPays
     * @return l'équipe créée.
     */
    private Equipe creerEquipe(String nomPays) {
        Equipe e = new Equipe(nomPays);
        // On crée des coordonnées par défaut. Elles seront réattrbuées plus tard.
        Base base = new Base(new Coordonnees(0, 0), e);
        e.setBase(base);

        return e;
    }

    public void setNetworkGame(boolean networkGame) {
        this.networkGame = networkGame;
    }

    /**
     * Demande le nombre d'équipe à créer pour du réseau.
     */
    private int demanderNombreEquipeReseau(int nb) {
        if (networkGame == false) {
            return 0;
        }

        return demanderNombre("Combien d'équipes distantes ?", 1, nb - 1, "Entrez un nombre valide !");
    }

    void setNetworkInformations(InfoTransfert info) {
        this.networkInformations = info;
    }

    /**
     * Permet de demander au joueur les différentes informations.
     */
    private void completerPartieInformation() {

        afficheFenetre();
        
        int numEquipe = networkInformations.getNumEquipe();

        /**
         * Jeu
         */
        SessionJeu SJ = networkInformations.getSession();

        Jeu.getSJ().setEquipes(SJ.getEquipes());
        Jeu.getSJ().setNbRobotsEquipes(SJ.getNbRobotsEquipes());
        Jeu.getSJ().setPlateau(SJ.getPlateau());

        Equipe monEquipe = SJ.getEquipe(numEquipe);

        // On modifie les caractéristiques pour dire qu'il s'agit d'équipe distantes.
        for (Equipe e : SJ.getEquipes()) {
            e.setIsJoueurReseau((e.getNumero() != numEquipe));
        }

        String nouveauNom = demanderNom(new ArrayList<String>(), 0);
        monEquipe.setPays(nouveauNom);

        constituerEquipe(monEquipe);

        // Doit notifier le serveur des changements...
        Jeu.getSJ().getConnexionClient().envoyer(new InfoTransfert("donneesEquipe", numEquipe, monEquipe));
        Jeu.lancerJeu();
    }

    /**
     * Demande le nom du pays.
     *
     * @param lesPays
     * @param i
     * @return
     */
    private String demanderNom(List<String> lesPays, int i) {
        if(Constante.debug_IA==true){
            return "1111";
        }
        String nomPays = "";
        do {
            // on affiche une erreur si le nom est déjà utilisé.
            if (lesPays.contains(nomPays.toLowerCase())) {
                afficherErreur("Zut ! Ce nom est déjà utilisé !");
            } else if (nomPays.length() > 0) {
                afficherErreur("Utilisez un nom de plus de 3 caractères !");
            }

            System.out.println("Entrez le nom du pays " + (i + 1) + " (carac. > 2) : ");
            nomPays = scan.next();
            
            // On remplace juste les espaces par des tirets bas pour éviter les soucis
            nomPays = nomPays.replace(" ", "_");

        } while (nomPays.length() <= 2 || lesPays.contains(nomPays.toLowerCase()));

        return nomPays;
    }

    /**
     * Lance un serveur réseau
     */
    private void lancerServeurReseau() {
        if(networkGame == false){
            return;
        }
        
        Jeu.getSJ().setServeur(new Serveur(Constante.RESEAU_PORT));
        Jeu.getSJ().getServeur().lancerServeur();
        Textes.printErr("Lancement du serveur...");
    }

}
