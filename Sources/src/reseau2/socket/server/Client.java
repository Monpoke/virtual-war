package reseau2.socket.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import outils.InfoTransfert;
import outils.Textes;
import virtualwar.fenetres.Jeu;

/**
 * Représente un client pour le serveur.
 *
 * @author Pierre Bourgeois
 */
public class Client implements Runnable {

    private final ServerSocket ss;
    private final Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private final ConnexionClient client;

    /**
     * Cette variable vérifie que le client est opérationnel.
     */
    private boolean estOperationnel = false;
    private int numeroEquipe;

    Client(ServerSocket ss, Socket socket, ConnexionClient client, int numeroEquipe) {
        this.ss = ss;
        this.socket = socket;
        this.client = client;
        this.numeroEquipe = numeroEquipe;

        /**
         * On envoit des objets
         */
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Textes.printErr("IO error: " + ex.getMessage());
        }
    }

    @Override
    public void run() {

        Thread receptionDuClientPourServeur = new Thread(new EcouterClient(in, socket, this));
        receptionDuClientPourServeur.start();

    }

    /**
     * Repère la déconnexion du socket.
     */
    public void disconnectSocket() {
        try {
            in.close();
            socket.close();

            /**
             * On notifie le serveur de la déconnexion.
             */
            client.getServeur().deconnexionClient(socket);

            Textes.printErr("Un client s'est déconnecté.");
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Vérifie si le client est prêt.
     *
     * @return
     */
    public boolean estOperationnel() {
        return estOperationnel;
    }

    /**
     * Détermine si le client est prêt.
     *
     * @param estOperationnel
     */
    public void setEstOperationnel(boolean estOperationnel) {
        this.estOperationnel = estOperationnel;
    }

    /**
     * Cette fonction permet d'envoyer les données de la partie au client
     */
    public void envoyerDonneesPartie() {
        try {
            // Quel numéro ?
            out.writeObject(new InfoTransfert("init", numeroEquipe, Jeu.getSJ()));
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void transfertInfos(InfoTransfert infos){
        try {
            // On transmet l'info
            out.writeObject(infos);
            out.flush();
            Textes.printErr("Client informé!");
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Retourne la socket client.
     *
     * @return
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * On lui donne un numéro d'équipe.
     *
     * @param numeroEquipe
     */
    public void setNumeroEquipe(int numeroEquipe) {
        this.numeroEquipe = numeroEquipe;
    }

    /**
     * Récupère le numéro d'équipe.
     *
     * @return
     */
    public int getNumeroEquipe() {
        return numeroEquipe;
    }

}
