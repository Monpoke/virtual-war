package virtualwar;

import virtualwar.general.Coordonnees;
import virtualwar.plateau.Base;
import virtualwar.robots.Char;
import virtualwar.robots.Robot;
import virtualwar.equipes.Equipe;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author youdelice
 */

public class BaseTest {
    
   @Test
    private void testgetEquipe(){
        System.out.println("getEquipe");
        Coordonnees coord = new Coordonnees(0, 0);
        Equipe instance = new Equipe("france");
        Base base = new Base(coord , instance);
        assertEquals("france",base.getEquipe().getPays());
        assertEquals(coord, base.getCoordonnees());
    }
    @Test
    private void testgetrobot(){
        System.out.println("getRobot");
        Coordonnees coord = new Coordonnees(0, 0);
        
        Equipe instance = new Equipe("france");
        
        Base base = new Base(coord , instance);
        
        Robot chars = new Char(instance);
        instance.ajoutRobot(chars);
        
        //assertTrue(base.getRobots().contains(chars));
    }
   
    @Test
    private void testPeutSeDeplacerSur(){
        System.out.println("getRobot");
        Coordonnees coord = new Coordonnees(0, 0);
        
        Equipe instance = new Equipe("france");
        
        Base base = new Base(coord , instance);
        
        Robot chars = new Char(instance);
        
    }
   
   
}