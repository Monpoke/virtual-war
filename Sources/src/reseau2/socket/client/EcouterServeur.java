package reseau2.socket.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import outils.InfoTransfert;
import outils.Textes;
import virtualwar.fenetres.Jeu;

/**
 * Cette classe permet d'écouter le serveur.
 *
 * @author Pierre Bourgeois
 */
class EcouterServeur implements Runnable {

    private final ConnexionClient client;
    private final ObjectInputStream in;

    /**
     * Se base par rapport à un client et un flux d'entrée.
     *
     * @param client
     * @param in
     */
    public EcouterServeur(ConnexionClient client, ObjectInputStream in) {
        this.client = client;
        this.in = in;
    }

    @Override
    public void run() {
        boolean connecte = true;

        while (connecte) {
            try {
                Textes.printErr("On réécoute...");
                final InfoTransfert info = (InfoTransfert) in.readObject();

                Textes.printErr("RECU DU SERVEUR ! " + info.getType());

                // On vérifie si ce sont les données initiales de partie
                if (info.getType().equals("init")) {
                    Thread jeu = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            Jeu.demanderParametresPourReseau(info);
                        }
                    });
                    jeu.start();
                } else {
                    // On fait gérer ça par l'exécuteur réseau
                    ReseauExecuteur.execute(info);

                }

            } catch (IOException ex) {
                Textes.printErr("Hum... Erreur de la part du serveur ? " + ex.getMessage());
                try { 
                    in.close();
                    connecte = false;
                } catch (IOException ex1) {
                    Logger.getLogger(EcouterServeur.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (ClassNotFoundException ex) {
                Textes.printErr("Une erreur est survenue...");
            }
        }

    }

}
