
package virtualwar.plateau;

import java.awt.*;
import java.io.Serializable;
import virtualwar.equipes.Equipe;
import outils.PathFinding;
 
import java.util.Map;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;

/**
 * Le plateau de jeu contient toutes les cellules du jeu.
 *
 * @author Square N6
 */
public class Plateau implements Constante, Serializable  {

    /**
     * Cellules du plateau de jeu.
     */
    private Cellule[][] cellules;

    /**
     * Contient toutes les bases du plateau.
     */
    private Map<Equipe, Base> bases;

    /**
     * Attribut contenant le pourcentage d'obstacle du plateau de jeu.
     */
    private int pourcentageObstacles = 0;

    /**
     * Crée un plateau 10x15 lorsque la taille n'est pas spécifiée.
     *
     * @param equipes Contient les équipes.
     */
    public Plateau(Equipe[] equipes) {
        this(10, 15, equipes, 30);
    }

    /**
     * Crée un plateau de jeu de largeur et hauteur spécifiées.
     *
     * @param largeur               Largeur du plateau de jeu.
     * @param hauteur               Hauteur du plateau de jeu.
     * @param equipes               Une liste d'équipe.
     * @param pourcentageObstacles  Le pourcentage d'obstacle du plateau de jeu.
     */
    public Plateau(int largeur, int hauteur, Equipe[] equipes, int pourcentageObstacles) {
        this.cellules = new Cellule[hauteur][largeur];
        this.pourcentageObstacles = pourcentageObstacles;

        // Tant qu'il n'existe pas un chemin entre les bases.
        do {
            // Génère le terrain.
            for (int y = 0; y < hauteur; y++) {
                for (int x = 0; x < largeur; x++) {
                    cellules[y][x] = new Case(new Coordonnees(x, y));
                }
            }

            int nbObstacle = (int)((largeur * hauteur / 2) * (pourcentageObstacles / 100f));
            int x;
            int y;
            for (int i = 0; i < nbObstacle; i++) {
                do {
                    x = rand.nextInt(largeur);
                    y = rand.nextInt(hauteur);
                }
                while (cellules[y][x].contientObstacle() || !estDansZoneObstacle(x, y));
                ((Case) cellules[y][x]).setObstacle(new Obstacle());
            }

        }
        while (!PathFinding.chemin(cellules, cellules[0][0], cellules[hauteur - 1][largeur - 1], false)
                || equipes.length > 2 && !PathFinding.chemin(cellules, cellules[0][0], cellules[hauteur - 1][0], false)
                || equipes.length > 3 && !PathFinding.chemin(cellules, cellules[0][0], cellules[0][largeur - 1], false)
                );

        // Place les Bases
        Base base;
        // Première base
        base = new Base(new Coordonnees(0, 0), equipes[0]);
        equipes[0].setBase(base);
        cellules[0][0] = base;
        
        // Deuxième base
        base = new Base(new Coordonnees(largeur - 1, hauteur - 1), equipes[1]);
        equipes[1].setBase(base);
        cellules[hauteur - 1][largeur - 1] = base;
        // Troisième base s'il y a
        if (equipes.length > 2) {
            base = new Base(new Coordonnees(0, hauteur - 1), equipes[2]);
            equipes[2].setBase(base);
            cellules[hauteur - 1][0] = base;
            // Quatrième base s'il y a
            if (equipes.length > 3) {
                base = new Base(new Coordonnees(largeur - 1, 0), equipes[3]);
                equipes[3].setBase(base);
                cellules[0][largeur - 1] = base;
            }
        }
        
    }

    /**
     * Retourne true si le point de coordonnées passées en paramètres est dans la zone ou les obstacles peuvent être.
     *
     * @param x Coordonnée x du point.
     * @param y Coordonnée y du point.
     * @return  True si (x, y) est dans la zone d'obstacle, false sinon.
     */
    private boolean estDansZoneObstacle(int x, int y) {
        int largeur = getLargeurPlateau();
        int hauteur = getHauteurPlateau();
        int margeX = largeur / 7;
        int margeY = hauteur / 7;
        return Math.abs(x - (largeur - margeX) / 2.0) * hauteur + Math.abs(y - (hauteur - margeY) / 2.0) * largeur <= ((largeur + margeX) * (hauteur + margeY)) / 2.0;
    }

    /**
     * Retourne une représentation ASCII du plateau de jeu.
     *
     * @return Représentation ASCII du plateau de jeu.
     */
    @Override
    public String toString() {
        return Plateau.afficherPlateau(cellules);
    }
    
    /**
     * Interprète un tableau de cellules.
     * @param cellules  Les cellules du tableau
     * @return  Représentation ASCII du tableau
     */
    public static String afficherPlateau(Cellule[][] cellules){
        String plateau = "";
        
        for (int y = 0; y < cellules.length * 2 + 1; y++) {
            for (int x = 0; x < cellules[0].length * 2 + 1; x++) {
                if (y % 2 == 0) {
                    if (x % 2 == 0) {
                        plateau += "+";
                    } else {
                        plateau += "---";
                    }
                } else if (x % 2 == 0) {
                    plateau += "|";
                } else {
                    plateau += " " + cellules[y / 2][x / 2].toString() + " ";
                }
            }
            plateau += "\n";
        }
        
        return plateau;
    }

    /**
     * Retourne la largeur du plateau
     *
     * @return la largeur du plateau
     */
    public int getLargeurPlateau() {
        return cellules[0].length;
    }

    /**
     * Retourne la hauteur du plateau
     *
     * @return la hauteur du plateau
     */
    public int getHauteurPlateau() {
        return cellules.length;
    }

    /**
     * Return le pourcentage d'obstacle dans Plateau
     * @return int pourcentageObstacles 
     */
    public int getPourcentageObstacles() {
        return pourcentageObstacles;
    }

    /**
     * Setter du pourcentage d'obstacle dans Plateau
     * @param pourcentageObstacles 
     */
    public void setPourcentageObstacles(int pourcentageObstacles) {
        this.pourcentageObstacles = pourcentageObstacles;
    }

    /**
     * 
     * @return 
     */
    public Cellule[][] getPlateau(){
        return this.cellules;
    }
    
    /**
     * Return cellule par rapport aux parametres x,y
     * @param x
     * @param y
     * @return this.cellules[y][x]
     */
    public Cellule getCellule(int x,int y){
        return this.cellules[y][x];
    }
    
    /**
     * Getter cellule
     * @param coord
     * @return cellule in parameter
     */
    public Cellule getCellule(Coordonnees coord){
        return this.getCellule(coord.getX(),coord.getY());
    }
    
    /**
     * 
     * 
     * @param coord
     * @return true si la cellule en parametre ne contient pas d'obstacle et de robot, false sinon
     */
    public boolean estLibre(Coordonnees coord){
        return getCellule(coord).estLibre();
    }
    /*
    return true si la cellule se trouve dans le plateau, faux autrement
    Sert à éviter les outOfBorn.
    */
    public boolean estDansPlateau(Coordonnees coord){
        return coord.getX()>=0 && coord.getY()>=0 && coord.getX()<getLargeurPlateau() && coord.getY()<getHauteurPlateau();
    }

}
