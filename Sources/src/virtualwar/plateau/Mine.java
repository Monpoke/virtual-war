package virtualwar.plateau;

import java.io.Serializable;
import virtualwar.equipes.Equipe;
 
/**
 * Cette classe représente une mine.
 *
 * @author Florent Marcaille, Pierre Bourgeois
 */
public class Mine implements Serializable {

    /**
     * Type de la mine
     */
    private String type;

    /**
     * Qui a posé la mine
     */
    private Equipe equipe;

    /**
     * Détermine l'équipe qui a posé la mine.
     *
     * @param equipe l'equipe de la mine
     */
    public Mine(Equipe equipe) {
        this.equipe = equipe;
    }

    /**
     * Retourne le type de la mine. Ce champ est prévu pour être modifié plus
     * tard pour faire différent types de mine.
     *
     * @return mine
     */
    public String getType() {
        return "mine";
    }

    /**
     * Retourne l'équipe associée à la mine.
     *
     * @return une équipe.
     */
    public Equipe getEquipe() {
        return equipe;
    }

}
