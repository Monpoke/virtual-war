package virtualwar.fenetres;

import java.io.Serializable;
import outils.InfoTransfert;
import outils.Textes;
import reseau2.socket.client.ConnexionClient;
import reseau2.socket.server.Serveur;
import virtualwar.equipes.Equipe;
import virtualwar.plateau.Plateau;

/**
 * Cette classe représente toute les données associées à une partie en cours.
 *
 * @author Pierre Bourgeois
 */
public class SessionJeu implements Serializable {

    /**
     * Les différentes équipes en jeu.
     */
    private Equipe[] equipes;

    /**
     * Nombre de robots par équipe.
     */
    private int nbRobotsEquipes = 0;
    /**
     * Contient le plateau de jeu.
     */
    private Plateau plateau;

    private int tourActuel = 0;

    /**
     * Tout ce qui concerne le réseau
     */
    private transient Serveur serveur;
    private transient ConnexionClient connexionClient;

    /**
     * Constructeur.
     */
    public SessionJeu() {
        System.out.println("Nouvelle session de jeu.");

    }

    /**
     * Retourne les équipes.
     *
     * @return tableau d'équipe.
     */
    public Equipe[] getEquipes() {
        return equipes;
    }

    /**
     * Retourne une équipe par sa place dans le tableau.
     *
     * @param nb une place dans le tableau.
     * @return une équipe.
     */
    public Equipe getEquipe(int nb) {
        return equipes[nb % equipes.length];
    }

    /**
     * Attribue les équipes.
     *
     * @param equipes des équipes.
     */
    public void setEquipes(Equipe[] equipes) {
        this.equipes = equipes;
    }

    /**
     * Retourne le plateau.
     *
     * @return
     */
    public Plateau getPlateau() {
        return plateau;
    }

    /**
     * Enregistre le plateau.
     *
     * @param plateau un plateau.
     */
    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    /**
     * Retourne le nombre de robots par équipe.
     *
     * @return un nombre.
     */
    public int getNbRobotsEquipes() {
        return nbRobotsEquipes;
    }

    /**
     * Attribue le nombre de robots par équipe.
     *
     * @param nbRobotsEquipes
     */
    public void setNbRobotsEquipes(int nbRobotsEquipes) {
        this.nbRobotsEquipes = nbRobotsEquipes;
    }

    /**
     * Retourne le serveur qui héberge.
     *
     * @return
     */
    public Serveur getServeur() {
        return serveur;
    }

    public void setServeur(Serveur serveur) {
        this.serveur = serveur;
    }

    public boolean estPartieReseau() {
        return serveur != null || connexionClient != null;
    }
    
    /**
     * Permet de notifier le serveur d'une action
     * @param infos 
     */
    public void notifierServeur(InfoTransfert infos){
        if(!estPartieReseau()){
            return;
        }
        
        // On vérifie que le joueur est le serveur 
        if(getServeur() != null){
            getServeur().notifierInfos(infos);
        } 
        // sinon on l'envoie
        else {
            getConnexionClient().envoyer(infos);
        }
        
        Textes.printErr("ENVOYE AU SERVEUR");
    }

    public int getTourActuel() {
        return tourActuel;
    }

    public void tourSuivant() {
        this.tourActuel++;
    }

    /**
     *
     * @param connexionClient
     */
    public void setClientReseau(ConnexionClient connexionClient) {
        this.connexionClient = connexionClient;
    }

    public ConnexionClient getConnexionClient() {
        return connexionClient;
    }

    /**
     * Retourne le nombre d'équipe réseau.
     *
     * @return
     */
    public int getNbEquipeDistantes() {
        int nb = 0;
        if (equipes == null) {
            return nb;
        }
        
        for (Equipe e : equipes) {
            if (e.isJoueurReseau()) {
                nb++;
            }
        }
        return nb;
    }

}
