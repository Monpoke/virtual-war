package outils;

import java.io.Serializable;
import reseau2.socket.server.Client;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.fenetres.SessionJeu;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Plateau;
import virtualwar.robots.Robot;

/**
 * Cette classe est utilisée pour transférer des classes en réseau.
 *
 * @author Pierre
 */
public class InfoTransfert implements Serializable {

    private SessionJeu session = null;
    private Equipe equipe = null;
    private String type;
    private int numEquipe;
    private Robot robot = null;
    private Coordonnees direction = null;

    // Pour transfert
    private transient Client clientOriginal = null;
    private transient boolean isLocal = false;

    private InfoTransfert(String type, int numEquipe) {
        this.type = type;
        this.numEquipe = numEquipe;
        this.session = Jeu.getSJ();
    }

    public InfoTransfert(String type, int numEquipe, SessionJeu session) {
        this(type, numEquipe);

    }

    public InfoTransfert(String type, int numEquipe, Equipe eq) {
        this(type, numEquipe);
        this.equipe = eq;
    }

    public Robot getRobot() {
        return robot;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public Coordonnees getDirection() {
        return direction;
    }

    public void setDirection(Coordonnees direction) {
        this.direction = direction;
    }

    /**
     * Retourne la session de jeu.
     *
     * @return
     */
    public SessionJeu getSession() {
        return session;
    }

    public void setSession(SessionJeu session) {
        this.session = session;
    }

    /**
     * Retourne le type d'info que l'instance contient.
     *
     * @return
     */
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Retourne le numéro d'équipe pour qui l'instance est destinée.
     *
     * @return
     */
    public int getNumEquipe() {
        return numEquipe;
    }

    @Override
    public String toString() {
        return "Id equipe: " + numEquipe + ", type: " + type;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    /**
     *
     * @param client
     */
    public void setClientOriginal(Client client) {
        this.clientOriginal = client;
    }

    public Client getClientOriginal() {
        return clientOriginal;
    }

    /**
     * Retourne un booléen si l'action est locale
     *
     * @return
     */
    public boolean fromLocal() {
        return isLocal;
    }

    /**
     * Détermine si c'est de l'action locale
     *
     * @param b
     */
    public void isLocal(boolean b) {
        this.isLocal = b;
    }

}
