package virtualwar.fenetres;

import java.util.Scanner;
import outils.Textes;

/**
 * Cette classe reprend des méthodes générales pour toutes les sous-fenêtres du
 * jeu.
 *
 * @author Pierre Bourgeois
 */
public abstract class Fenetre {

    /**
     * Nom de la fenêtre
     */
    private String nom;

    /**
     * Scanner utilisé par les sous-fenêtres.
     */
    protected Scanner scan;

    /**
     * Constructeur.
     *
     * @param nom Nom de la fenêtre.
     */
    public Fenetre(String nom) {
        this.nom = nom;
        scan = new Scanner(System.in);
    }

    /**
     * Nom de la fenêtre.
     *
     * @return un nom.
     */
    public String getNom() {
        return nom;
    }

    /**
     * Modifie le nom de la fenêtre.
     *
     * @param nom un nom.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Méthode abtraite pour toutes les fenêtres.
     */
    public abstract void lancer();

    /**
     * Affiche une fenêtre
     */
    protected void afficheFenetre() {
        effacerEcran();
        System.out.println("---------------------------------\n--       VIRTUAL WAR\n-- [Fenêtre: " + this.getNom() + "]");
        if (Jeu.getSJ().estPartieReseau()) {
            System.out.println("--    <PARTIE RESEAU>");
        }
        System.out.println("---------------------------------\n");

    }

    /**
     * Effacer les écrans.
     */
    protected void effacerEcran() {
        for (int i = 0; i < 30; i++) {
            System.out.println();
        }
    }

    /**
     * Convertit un String vers un Integer
     *
     * @param input Le texte à convertir
     * @param badValue Le nombre à retourner par défaut.
     * @return un nombre.
     */
    protected int convertStringToInt(String input, int badValue) {
        int aRetourner;

        try {
            aRetourner = Integer.parseInt(input);
        } catch (NumberFormatException numberFormatException) {
            aRetourner = badValue;
        }

        return aRetourner;
    }

    /**
     * Demande un nombre au joueur.
     *
     * @param question la question qui sera posée
     * @param min un minimum
     * @param max un maximum
     * @param messageErreur le message d'erreur renvoyé lors d'une erreur de
     * saisie
     * @return un nombre compris entre min et max
     */
    protected int demanderNombre(String question, int min, int max, String messageErreur) {

        String input = "";
        int re = min - 1;
        // Nombre d'équipes
        do {

            if (input.length() > 0) {
                afficherErreur(messageErreur);
            }
            
            System.out.println(question + " (" + min + "-" + max + ")");
            input = scan.next();

            re = convertStringToInt(input, min - 1);
            
        } while (!(re >= min && re <= max));

        return re;
    }

    /**
     * Affiche une erreur à l'utilisateur de l'application.
     *
     * @param message un message.
     */
    protected void afficherErreur(String message) {
        System.out.println("ERREUR: " + message);
    }

}
