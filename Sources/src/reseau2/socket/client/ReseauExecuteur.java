/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reseau2.socket.client;

import outils.InfoTransfert;
import outils.Textes;
import virtualwar.actions.Deplacement;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.fenetres.SessionJeu;
import virtualwar.robots.Robot;

/**
 * Cette classe execute localement les actions menées par les utilisateurs
 * réseau
 *
 * @author Pierre
 */
public class ReseauExecuteur {

    /**
     * Execute info
     *
     * @param info
     */
    public static void execute(InfoTransfert info) {
        if (info.getType().equals("deplacement")) {
            Robot local = getRobotLocal(info.getRobot());

            Textes.printErr(info.getDirection().toString());
            Deplacement d = new Deplacement(local, info.getDirection());
            d.isNetworkAction();
            d.agit();
            
            Textes.printErr("DEPLACEMENT");
            

        }

        // reproduit le session jeu
        reproduitSJ(info.getSession());
    }


    // Get a local robot
    private static Robot getRobotLocal(Robot robot) {
        for (Equipe equipe : Jeu.getSJ().getEquipes()) {
            for (Robot robot_ : equipe.getRobots()) {
                if (robot.equals(robot_)) {
                    return robot_;
                }
            }
        }

        return null;
    }

    private static void reproduitSJ(SessionJeu session) {
        SessionJeu local = Jeu.getSJ();

        // tour
        local.tourSuivant();

        // Reproduction des tours de jeu
        for (Equipe equipeDistante : session.getEquipes()) {
            
        }
    }
}
