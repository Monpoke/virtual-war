package virtualwar.actions;

import outils.InfoTransfert;
import virtualwar.fenetres.Jeu;
import virtualwar.general.Coordonnees;
import virtualwar.robots.Robot;

/**
 * Représente la classe mère pour toutes les actions. Déplacement et Attaque.
 *
 * @author Square N6
 */
public abstract class Action {

    /**
     * Le robot effectuant l'action.
     */
    private Robot robot;

    /**
     * La direction de l'action.
     */
    private Coordonnees direction;
    
    /**
     * Détermine si c'est une action réseau
     */
    private boolean network = false;

    /**
     * Constructeur de la classe mère.
     *
     * @param robot type de robot
     * @param direction coordonnée de l'action
     */
    public Action(Robot robot, Coordonnees direction) {
        this.robot = robot;
        this.direction = direction;
    }

    /**
     * Méthode effectuant l'action
     *
     */
    public abstract void agit();

    
    /**
     * Retourne le robot qui effectue l'action.
     * 
     * @return le robot qui agit.
     */
    public Robot getRobot() {
        return robot;
    }

    /**
     * Retourne un vecteur de direction.
     * @return un vecteur.
     */
    public Coordonnees getDirection() {
        return direction;
    }
    
    
    /**
     * Détermine si l'action est à partir du réseau
     */
    public void isNetworkAction() {
        this.network = true;
    }

    public boolean isNetwork() {
        return network;
    }
    
    protected void notifierServeur(String type){
        if (!isNetwork()) {
            // Notification au serveur
            InfoTransfert infos = new InfoTransfert(type, getRobot().getEquipe().getNumero(), Jeu.getSJ());
            infos.setRobot(getRobot());
            infos.setDirection(getDirection());
            infos.isLocal(true);
            Jeu.getSJ().notifierServeur(infos);
        }
    }
    
}
