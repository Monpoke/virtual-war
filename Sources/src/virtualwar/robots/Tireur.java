
package virtualwar.robots;

import virtualwar.equipes.Equipe;
import java.util.List;
import java.util.ArrayList;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
/**
 *
 * @author Florent
 */
public class Tireur extends Robot{

/**
 * constructeur d'un tireur
 * 
 * @param equipe    l'equipe d'un tireur
 */
    public Tireur(Equipe equipe) {
        super(equipe);
        setEnergie(Constante.TIREUR.get("energie"));
    }
    /**
     * return le cout d'une action d'un tireur
     * 
     * @return le cout d'une action d'un tireur
     */
    @Override
    public int getCoutAction(){
        return Constante.TIREUR.get("tirer");
    }

    
    /**
     * return le cout d'un deplacement d'un tireur
     *
     * @return le cout d'un deplacement d'un tireur
     */

    @Override
    public int getCoutDeplacement(){
        return Constante.TIREUR.get("deplacement");
    }

    
    /**
     * return le degat d'un tir d'un tireur
     * 
     * @return le degat d'un tir d'un tireur
     */

    @Override
    public int getDegatTir(){
        return Constante.TIREUR.get("degat");
    }




    /**
     * return le degat d'une mine d'un tireur
     * 
     * @return le degat d'une mine d'un tireur
     */

    @Override
    public int getDegatMine(){
        return 0;
    }
    
    /**
     * return la porté d'un tireur
     * 
     * @return la porté d'un tireur
     */
    
    @Override
    public int getPortee() {
        return Constante.PIEGEUR.get("portee");
    }
    
    
    /**
     * return le type du tireur
     * 
     * @return tireur
     */

    @Override
    public String getType(){
        return "tireur";
    }

    
    /**
     * return la liste des direction que le tireur a pour se deplacer
     * 
     * @return la liste des direction que le tireur a pour se deplacer
     */
    @Override
    public List<Coordonnees> getDirectionsPossibles(){
        List<Coordonnees> listeCoordonnees = new ArrayList<>();
        
        listeCoordonnees.add(Constante.dep.get(0));     //  HAUT
        listeCoordonnees.add(Constante.dep.get(1));     //  HAUT_DROITE
        listeCoordonnees.add(Constante.dep.get(2));     //  DROITE
        listeCoordonnees.add(Constante.dep.get(3));     //  BAS_DROITE
        listeCoordonnees.add(Constante.dep.get(4));     //  BAS
        listeCoordonnees.add(Constante.dep.get(5));     //  BAS_GAUCHE
        listeCoordonnees.add(Constante.dep.get(6));     //  GAUCHE
        listeCoordonnees.add(Constante.dep.get(7));     //  HAUT_GAUCHE
        
        return listeCoordonnees;
    } 
    
    
    /**
     * return la liste des direction que le piegeur a pour agir
     * QUI EST LE BOULET QUI A FAIT CETTE FONCTION ?!
     * @return la liste des directions que le piegeur a pour agir
     */
    @Override
    public List<Coordonnees> getActionDirectionsPossibles(){
        List<Coordonnees> listeCoordonnees = new ArrayList<>();
        
        listeCoordonnees.add(Constante.dep.get(0));     //  HAUT
        listeCoordonnees.add(Constante.dep.get(2));     //  DROITE
        listeCoordonnees.add(Constante.dep.get(4));     //  BAS
        listeCoordonnees.add(Constante.dep.get(6));     //  GAUCHE
        
        return listeCoordonnees;
    }    
    
    
    /**
     * On recharge l'énergie du Tireur.
     *
     * @param recupEnergie l'énergie à ajouter.
     */
    @Override
    public void rechargerEnergie(int recupEnergie) {
        super.rechargerEnergie(recupEnergie);
        if(this.getEnergie() > Constante.TIREUR.get("energie")){
            this.setEnergie(Constante.TIREUR.get("energie"));
        }
    }
}
