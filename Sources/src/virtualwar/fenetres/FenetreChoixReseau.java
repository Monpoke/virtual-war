/*
 *  Pierre Bourgeois - 2014
 */
package virtualwar.fenetres;

import java.io.IOException;
import java.net.Socket;
import outils.Textes;
import reseau2.socket.client.ConnexionClient;
import reseau2.socket.server.Serveur;
import virtualwar.general.Constante;

/**
 * Demande les paramètres pour la partie en cours.
 *
 * @author Pierre Bourgeois
 */
public class FenetreChoixReseau extends Fenetre {

    boolean networkGame = false;

    public FenetreChoixReseau() {
        super("Partie en réseau");

    }

    /**
     * Code principale de la fenêtre.
     */
    @Override
    public void lancer() {
        afficheFenetre();

        System.out.println("Vous pouvez rejoindre ou héberger une partie. Que voulez vous faire ?");
        System.out.println("1) Héberger une partie.");
        System.out.println("2) Rejoindre une partie.");

        int choix = demanderNombre("Choix ?", 1, 2, "Faites un choix valide !");

        switch (choix) {
            case 1:
                creerNouvellePartie();
                break;
            case 2:
            default:
                rejoindrePartie();
                break;
        }
    }

    /**
     * Hébergement de nouvelle partie.
     */
    private void creerNouvellePartie() {
        // On demande quand même pour les paramètres de la partie.
        Jeu.creerPartieReseau();
    }

    /**
     * Veut rejoindre une partie.
     */
    private void rejoindrePartie() {

        /**
         * Quelle est l'ip ?
         */
        String ip = "";
        do {
            if (ip.length() > 0) {
                afficherErreur("L'IP donnée est invalide !");
            }

            System.out.println("Quelle est l'IP du serveur ?");
            ip = scan.next();
        } while (!ip.matches(Constante.IP_PATTERN));

        /**
         * Test de connexion
         */
        try {
            Socket socketClient = new Socket(ip, Constante.RESEAU_PORT);
            ConnexionClient connexionClient = new ConnexionClient(socketClient);
            Jeu.getSJ().setClientReseau(connexionClient);
            Thread client = new Thread(connexionClient);
            client.start();
            
        } catch (IOException ex) {
            System.out.println("Désolé, le serveur est injoignable...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            Jeu.lancer();
        }

    }
}
