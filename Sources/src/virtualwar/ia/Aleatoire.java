
package virtualwar.ia;

import java.util.Random;

import outils.Textes;
import virtualwar.actions.Action;
import virtualwar.actions.Attaque;
import virtualwar.general.Coordonnees;
import virtualwar.actions.Deplacement;
import virtualwar.equipes.Equipe;
import virtualwar.robots.Robot;
import virtualwar.robots.Tireur;


/**
 *
 * @author Pierre
 */
public class Aleatoire extends IA {


    public Aleatoire(Equipe e) {
        super(e);
        System.out.println("Création d'une IA");
    }


    @Override
    public void choisirRobots(int nbRobots) {

        // Ajout de tireur pour le moment
        for(int i = 0; i < nbRobots; i++){
            getEquipe().ajoutRobot(new Tireur(getEquipe()));
        }
       
    }

    
    @Override
    public String jouer() {
        String retour = "";
        
        // On choisit un robot au hasard
        Random rnd = new Random();
        
        Robot robot = getEquipe().getRobots().get(rnd.nextInt(getEquipe().getRobots().size()));
        
        Coordonnees direction, testDirection;
        
        if(rnd.nextInt(2)==0){
        	 do {
        		 direction = robot.getDirectionsPossibles().get(rnd.nextInt(robot.getDirectionsPossibles().size()));
                 retour = "Je tire vers " + direction;
                 
                 testDirection = robot.getCoordonnees().ajoutVecteur(direction);
             } while(!robot.tireOK(testDirection));
             
             
             System.err.println("Coords Base equipe: "+ getEquipe().getBase().getCoordonnees());
             System.err.println("Je peux tirer vers " + direction);
             
             Action tire = new Attaque(robot, direction);
             tire.agit();
        }
        else{
        	
            do {
                direction = robot.getDirectionsPossibles().get(rnd.nextInt(robot.getDirectionsPossibles().size()));
                retour = "Je vais vers " + direction;
                
                testDirection = robot.getCoordonnees().ajoutVecteur(direction);
            } while(!getEquipe().getVue().deplacementOK(robot, testDirection));
            
            
            System.err.println("Coords Base equipe: "+ getEquipe().getBase().getCoordonnees());
            System.err.println("Je peux me déplacer vers " + direction);
            
            Action depla = new Deplacement(robot, direction);
            depla.agit();
        }
        return retour;
    }
    
}
