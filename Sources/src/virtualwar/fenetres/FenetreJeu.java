/*
 *  Pierre Bourgeois - 2014
 */
package virtualwar.fenetres;

import virtualwar.plateau.Cellule;
import virtualwar.plateau.Plateau;
import virtualwar.robots.Piegeur;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import outils.Textes;
import reseau2.socket.server.Serveur;
import virtualwar.actions.Action;
import virtualwar.actions.Attaque;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.actions.Deplacement;
import virtualwar.equipes.Equipe;
import virtualwar.robots.Robot;

/**
 * Cette classe représente les possibilités du jeu.
 *
 * @author Pierre Bourgeois
 * @since 2015-03-25
 * @version 0.0.0
 */
public class FenetreJeu extends Fenetre {

    private boolean jeuTermine = false;

    public FenetreJeu() {
        super("Jeu en cours.");
    }

    /**
     * Cette fonction permet de lancer une nouvelle partie.
     */
    @Override
    public void lancer() {

        /**
         * On initialise le premier tour.
         */
        boolean wasNetworkAction = false;
        do {

            verifierReseau();

            // On récupère le pays qui va jouer.
            Equipe equipeQuiJoue = getTourEquipe();
            equipeQuiJoue.restaurationDuTour();

            // On affiche la fenêtre.
            afficherFenetre();
            System.out.println("C'est au tour du pays `" + equipeQuiJoue.getPays() + "` de combattre !");

            /**
             * On vérifie que le joueur est une IA ou un joueur réseau.
             */
            if (equipeQuiJoue.isJoueurReseau()) {
                System.out.println("On doit attendre le joueur réseau.");
                wasNetworkAction = true;
                while (!isTourJoueur()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(FenetreJeu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            } else if (equipeQuiJoue.isIA()) {
                System.out.println(equipeQuiJoue.getIa().jouer());
            } else {

                boolean recommencer;
                do {
                    // Demande le robot
                    Robot robotAction = demanderRobot(equipeQuiJoue);

                    /**
                     * Demande le mouvement. Si l'action renvoie FAUX, alors on
                     * recommence le choix !
                     */
                    recommencer = !demanderAction(robotAction);
                } while (recommencer == true);

            }

            equipeQuiJoue.supprimerLesMorts();
            this.verifierJeuTermine(equipeQuiJoue);

            if (!wasNetworkAction) {
                Jeu.getSJ().tourSuivant();
            }
            wasNetworkAction = false;
        } while (!jeuTermine);

    }

    /**
     * On affiche la fenêtre de jeu.
     */
    private void afficherFenetre() {
        afficheFenetre();
        Textes.printErr("On nous demande d'afficher la fenêtre.");
        /**
         * On récupère l'équipe qui joue.
         */
        Equipe equipeActuelle = getTourEquipe();

        String plateau = Plateau.afficherPlateau(equipeActuelle.getVue().getPlateauFiltre());

        String menuDroite
                = "   .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-.     .-. \n"
                + " .'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `._.'   `.\n"
                + "VIRTUAL WAR - Partie en cours - " + Jeu.getSJ().getEquipes().length + " pays - Tour n°" + Jeu.getSJ().getTourActuel() + "\n\n";

        // On ajoute les équipes
        for (Equipe equipe : Jeu.getSJ().getEquipes()) {
            int nbRobots = equipe.getNbRobotsOperationnels();
            String etatEquipe = "Pays `" + equipe.getPays() + "` - " + Textes.genererPluriel(nbRobots, "Un robot", nbRobots + " robots", "Aucun robot")
                    + " - Base (" + equipe.getBase().getCoordonnees() + ")"
                    + "\n";

            for (Robot robot : equipe.getRobots()) {
                etatEquipe += "\t" + robot.getType();

                // On vérifie si le robot n'est pas mort.
                if (!robot.estMort()) {
                    etatEquipe += " (" + robot.getCoordonnees().getX() + "," + robot.getCoordonnees().getY() + ") - Energie: " + robot.getEnergie() + " - Id: " + robot.getNom();
                }

                etatEquipe += (robot.resumeEtat().length() > 0 ? " - " + robot.resumeEtat() : "")
                        + "\n";
            }

            menuDroite += etatEquipe + "\n";
        }

        // Ajout du menu droit au plateau.
        System.out.println(Textes.jointureDroite(plateau, menuDroite, 5));
    }

    /**
     * Retourne l'équipe qui joue actuellement.
     *
     * @return une équipe.
     */
    private Equipe getTourEquipe() {
        return Jeu.getSJ().getEquipe(Jeu.getSJ().getTourActuel());
    }

    /**
     * Demande le robot qui doit effectuer l'action.
     *
     * @param equipeQuiJoue
     * @return
     */
    private Robot demanderRobot(Equipe equipeQuiJoue) {

        Map<String, Robot> robots = new HashMap<>();
        String noms = "";
        // On affiche les robots qui ne sont pas morts
        for (Robot equiRobot : equipeQuiJoue.getRobots()) {
            if (!equiRobot.estMort()) {
                robots.put(equiRobot.getNom().toLowerCase(), equiRobot);
                noms += equiRobot.getNom() + ", ";
            }
        }

        if (noms.length() >= 2) {
            noms = noms.substring(0, noms.length() - 2);
        }

        // demande du nom
        String input = "";
        do {
            if (input.length() > 0) {
                afficherErreur("Ce robot n'existe pas.");
            }
            System.out.println("Quel robot voulez-vous utiliser ? (" + noms + ") - (Q) pour quitter le jeu");
            input = scan.next().toLowerCase();

        } while (!robots.containsKey(input) && !"q".equals(input));

        /**
         * Si le joueur veut quitter le jeu
         */
        if ("q".equals(input)) {
            termineJeu();
        }

        return robots.get(input);
    }

    /**
     * Demande l'action à effectuer par le robot.
     *
     * @param robotAction
     * @return
     */
    private boolean demanderAction(Robot robotAction) {

        String input = "";
        boolean resultAction = true; // Pour vérifier si quelque chose a été annulé
        boolean toReturn = true;
        do {

            boolean peutAgir = false, peutBouger = false;
            do {

                if (input.length() > 0) {
                    afficherErreur("L'action demandée n'existe pas !");
                }

                System.out.println("Quelle action voulez vous faire effectuer au robot `" + robotAction.getNom() + "` ?");

                if (robotAction.peutAgir()) {
                    System.out.print("Agir (a) ?");
                    peutAgir = true;
                }

                if (robotAction.peutBouger()) {
                    System.out.print(" Bouger (b) ?");
                    peutBouger = true;
                }

                if (Constante.DEBUG_MODE) {
                    System.out.print(" Cheat (x) ?");
                }

                System.out.println(" Annuler (z) ?");

                input = scan.next().toLowerCase();

            } while (!("a".equals(input) && peutAgir || "b".equals(input) && peutBouger || "z".equals(input) || Constante.DEBUG_MODE && "x".equals(input)));

            /**
             * Effectuer l'action demandée
             */
            toReturn = true;
            switch (input) {
                case "b":
                    resultAction = ouBouger(robotAction);
                    break;
                case "a":
                    resultAction = ouAgir(robotAction);
                    break;
                case "x":
                    demanderCheat(robotAction);
                    break;
                default:
                    toReturn = false;
                    resultAction = true;
            }
            input = "";
        } while (resultAction != true);

        return toReturn;
    }

    /**
     * Demande au joueur où le robot doit bouger.
     *
     * @param robotAction
     */
    private boolean ouBouger(Robot robotAction) {

        // On récupère les directions possibles pour se déplacer.
        List<Coordonnees> directionsPossibles = robotAction.getDirectionsPossibles();

        // On demande la direction sous la forme d'un vecteur
        Coordonnees correspondanceChoix = choisirDirection(directionsPossibles, "Dans quelle direction faut-il bouger ?");

        if (correspondanceChoix == null) {
            return false;
        }

        // On active le déplacement
        Action deplacement = new Deplacement(robotAction, correspondanceChoix);
        deplacement.agit();

        return true;
    }

    /**
     * Où le robot doit tirer ou miner.
     *
     * @param robotAction
     */
    private boolean ouAgir(Robot robotAction) {

        // On récupère les directions possibles pour se déplacer.
        List<Coordonnees> directionsPossibles = robotAction.getActionDirectionsPossibles();

        // On demande la direction sous la forme d'un vecteur
        Coordonnees correspondanceChoix = choisirDirection(directionsPossibles, "Dans quelle direction faut-il " + ((robotAction instanceof Piegeur) ? "poser une mine" : "tirer") + " ?");

        if (correspondanceChoix == null) {
            return false;
        }

        Action agir = new Attaque(robotAction, correspondanceChoix);
        agir.agit();

        return true;
    }

    /**
     * Demande au joueur la direction où agir.
     *
     * @param directionsPossibles
     * @return un vecteur de direction.
     */
    private Coordonnees choisirDirection(List<Coordonnees> directionsPossibles, String questionPosee) {
        HashMap<String, Coordonnees> correspondancesChoix = new HashMap<>();
        String choixDir = "", input = "";

        /**
         * Un moyen de raccourcir tout ça ?
         */
        for (Coordonnees dir : directionsPossibles) {
            if (dir == Constante.HAUT || dir == Constante.CHAR_HAUT) {
                choixDir += " Haut (h) ?";
                correspondancesChoix.put("h", dir);
            } else if (dir == Constante.DROITE || dir == Constante.CHAR_DROITE) {
                choixDir += " Droite (d) ?";
                correspondancesChoix.put("d", dir);
            } else if (dir == Constante.BAS || dir == Constante.CHAR_BAS) {
                choixDir += " Bas (b) ?";
                correspondancesChoix.put("b", dir);
            } else if (dir == Constante.GAUCHE || dir == Constante.CHAR_GAUCHE) {
                choixDir += " Gauche (g) ?";
                correspondancesChoix.put("g", dir);
            } else if (dir == Constante.HAUT_GAUCHE) {
                choixDir += " Haut-Gauche (hg) ?";
                correspondancesChoix.put("hg", dir);
            } else if (dir == Constante.HAUT_DROITE) {
                choixDir += " Haut-Droite (hd) ?";
                correspondancesChoix.put("hd", dir);
            } else if (dir == Constante.BAS_DROITE) {
                choixDir += " Bas-Droite (bd) ?";
                correspondancesChoix.put("bd", dir);
            } else if (dir == Constante.BAS_GAUCHE) {
                choixDir += " Bas-Gauche (bg) ?";
                correspondancesChoix.put("bg", dir);
            }
        }

        choixDir += " Annuler (z) ?";

        input = "";
        do {
            if (input.length() > 0) {
                afficherErreur("Saisissez une direction valide !");
            }

            System.out.println(questionPosee);
            System.out.println(choixDir);
            input = scan.next().toLowerCase();
        } while (!("z".equals(input) || correspondancesChoix.containsKey(input)));

        // On annule l'action
        if ("z".contains(input)) {
            return null;
        }

        return correspondancesChoix.get(input);
    }

    /**
     * Fonction particulièrement utile pour les cheats.
     *
     * @param robotAction
     */
    private void demanderCheat(Robot robotAction) {
        String input = "";
        do {
            System.out.println("Quel cheat ? (tuer, recharger, energie, deplacer)");
            input = scan.next();
        } while (!(input.equals("tuer") || input.equals("recharger") || input.equals("energie") || input.equals("deplacer")));

        // On vérifie
        if (input.contains("tuer")) {
            robotAction.setEnergie(0);
        } else if (input.contains("recharger")) {
            robotAction.rechargerEnergie(90000);
        } else if (input.contains("deplacer")) {

            int x = 0, y = 0;
            input = "";
            do {
                System.out.print("X: ");
                input = scan.next();
                x = convertStringToInt(input, -1);

                System.out.print("Y: ");
                input = scan.next();
                y = convertStringToInt(input, -1);
            } while (!(x >= 0 && x < Jeu.getSJ().getPlateau().getLargeurPlateau() && y >= 0 && y < Jeu.getSJ().getPlateau().getHauteurPlateau()));

            Cellule cellule = robotAction.getEquipe().getVue().getPlateau().getCellule(x, y);

            // On déplace le robot.
            robotAction.deplacerSur(cellule);

        } else if (input.contains("energie")) {

            int x = 0;
            input = "";
            do {
                System.out.print("Nouvelle energie: ");
                input = scan.next();
                x = convertStringToInt(input, -1);
            } while (!(x >= 0 && x < 100));

            robotAction.setEnergie(x);

        }
    }

    /**
     *
     * @param equipeQuiJoue
     */
    private void verifierJeuTermine(Equipe equipeQuiJoue) {
        /**
         * On vérifie qu'une équipe n'a plus de robot fonctionnel
         */
        List<Equipe> equipesFonctionnelles = new ArrayList<>();

        for (Equipe equipe : Jeu.getSJ().getEquipes()) {
            // on vérifie si l'équipe a au moins un robot fonctionnel
            for (Robot robot : equipe.getRobots()) {
                if (robot.peutBouger() || robot.peutAgir()) {
                    equipesFonctionnelles.add(equipe);
                    break;
                }
            }
        }

        /**
         * On vérifie le nombre d'équipe.
         */
        if (equipesFonctionnelles.size() <= 1) {
            effacerEcran();

            System.out.println("---------------------------\n----- GUERRE TERMINEE -----\n---------------------------");
            System.out.print("Pays remportant la guerre :");

            if (equipesFonctionnelles.size() == 1) {
                System.out.println(equipesFonctionnelles.get(0).getPays());
            } // dernière équipe.
            else {
                System.out.println(equipeQuiJoue.getPays());
            }

            jeuTermine = true;
        }
    }

    /**
     *
     */
    private void termineJeu() {
        jeuTermine = true;
        System.out.println("--------------------------------\nFIN DE PARTIE\n--------------------------------");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Textes.printErr(ex.getMessage());
        }

        Jeu.lancer();
    }

    /**
     * Cette fonction permet de vérifier des conditions particulières par
     * rapport au réseau.
     */
    private void verifierReseau() {

        /**
         * On vérifie que la partie est en réseau déjà.
         */
        if (!Jeu.getSJ().estPartieReseau() || Jeu.getSJ().getServeur() == null) {
            return;
        }

        /**
         * On vérifie les clients
         */
        Serveur serveur = Jeu.getSJ().getServeur();

        int nb = 1;
        while (serveur != null && serveur.getNombreConnectes() < nb) {
            afficheFenetre();
            System.out.println("En attente des joueurs...");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Textes.printErr("Tout le monde n'est pas encore connecté...");
            }
        }

        while (serveur != null && serveur.getNombreOperationnel() < nb) {
            afficheFenetre();
            System.out.println("Tout le monde n'est pas encore prêt...");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Textes.printErr("Tout le monde n'est pas encore connecté...");
            }
        }

    }

    /**
     * On vérifie si c'est au tour du joueur
     *
     * @return
     */
    private boolean isTourJoueur() {
        return !getTourEquipe().isJoueurReseau();
    }
}
