package virtualwar.fenetres;

/**
 * Représente la fenêtre d'aide.
 *
 * @author Pierre
 */
class FenetreAide extends Fenetre {

    /**
     * Initialise la fenêtre.
     */
    public FenetreAide() {
        super("Aide du jeu");
    }

    /**
     * Affiche la fenêtre d'aide.
     */
    @Override
    public void lancer() {
        effacerEcran();
        afficheFenetre();
        String gauche = "Pour commencer à jouer, sélectionnez l'option `Jouer` dans le menu principal.\n"
                + "Commencez par créer et composer les robots dans votre équipe.\n"
                + "Choisissez les méthodiquement, ils vous permettront de remporter la guerre !";

        String droite = "LES DIFFERENTS ROBOTS\n"
                + "CHAR - Se déplace de 2 cases dans les 4 cases adjacentes. Il possède une plus longue portée.\n"
                + "PIEGEUR - Se déplace d'une case dans les 8 directions. Il peut poser des mines.\n"
                + "TIREUR - Il tire sur les chars adverses.";

        System.out.println(gauche + "\n\n" + droite);

        System.out.println("\nAppuyez sur `Entrée` pour quitter l'aide.");
        String input = scan.nextLine();
        Jeu.lancer();
    }

}
