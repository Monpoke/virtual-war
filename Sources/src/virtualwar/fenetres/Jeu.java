package virtualwar.fenetres;

import outils.InfoTransfert;

/**
 * Cette classe représente le lanceur du jeu.
 *
 * @author Pierre Bourgeois
 */
public final class Jeu {

    /**
     * La fenêtre de jeu qui est lancée actuellement.
     */
    private static Fenetre fenetreActuelle;

    /**
     * Les paramètres de la partie.
     */
    private static SessionJeu paramsJeu;

    /**
     * Constructeur du jeu.
     */
    public Jeu() {

    }

    /**
     * Retourne la Session de Jeu.
     *
     * @return la session actuelle.
     */
    public static SessionJeu getSJ() {
        return paramsJeu;
    }

    /**
     * Lance la première fenêtre.
     */
    public static void lancer() {

        // Premières initialisations
        paramsJeu = new SessionJeu();

        // Première classe de jeu
        fenetreActuelle = new FenetreMenu();
        fenetreActuelle.lancer();
    }

    public static void demanderParametres() {
        fenetreActuelle = new FenetreParametresJeu();
        fenetreActuelle.lancer();
    }

    
    /**
     * Fonction appelée après avoir reçu les données initiales du serveur.
     * @param info 
     */
    public static void demanderParametresPourReseau(InfoTransfert info) {
        fenetreActuelle = new FenetreParametresJeu();
        ((FenetreParametresJeu) fenetreActuelle).setNetworkGame(true);
        ((FenetreParametresJeu) fenetreActuelle).setNetworkInformations(info);
        fenetreActuelle.lancer();
    }

    public static void creerPartieReseau() {
        fenetreActuelle = new FenetreParametresJeu();
        ((FenetreParametresJeu) fenetreActuelle).setNetworkGame(true);
        fenetreActuelle.lancer();
        
        
        
    }

    public static void partieReseau() {
        fenetreActuelle = new FenetreChoixReseau();
        fenetreActuelle.lancer();
    }

    public static void lancerJeu() {
        fenetreActuelle = new FenetreJeu();
        fenetreActuelle.lancer();
    }

    public static void afficherAide() {
        fenetreActuelle = new FenetreAide();
        fenetreActuelle.lancer();
    }
}
