/*
 *  Pierre Bourgeois - 2014
 */
package outils;

import virtualwar.general.Constante;

/**
 * Cette classe contient des méthodes utilitaires pour générer des pluriels, des
 * affichages...
 *
 * @author Pierre Bourgeois
 */
public abstract class Textes {

    /**
     * Retourne une phrase accordée selon le pluriel.
     *
     * @param nb Le nombre selon lequel la phrase doit être adaptée.
     * @param singulier La phrase singulière.
     * @param pluriel La phrase plurielle.
     * @param aucun La phrase vide.
     * @return La phrase adaptée.
     */
    public static String genererPluriel(int nb, String singulier, String pluriel, String aucun) {
        if (nb <= 0) {
            return aucun;
        } else if (nb == 1) {
            return singulier;
        } else {
            return pluriel;
        }
    }

    /**
     * Permet de coller une String à droite d'une autre String.
     *
     * @param p1 Texte à mettre à gauche.
     * @param p2 Texte à mettre à droite.
     * @param decalageParties Nombre d'espaces entre les 2 textes.
     * @return
     */
    public static String jointureDroite(String p1, String p2, int decalageParties) {
        return jointureDroite(p1, p2, decalageParties, 0);
    }

    /**
     * Permet de coller une String à droite d'une autre String.
     *
     * @param p1 Texte à mettre à gauche.
     * @param p2 Texte à mettre à droite.
     * @return
     */
    public static String jointureDroite(String p1, String p2) {
        return jointureDroite(p1, p2, 1, 0);
    }

    /**
     * Permet de coller une String à droite d'une autre String.
     *
     * @param p1 Texte à mettre à gauche.
     * @param p2 Texte à mettre à droite.
     * @param decalageParties Nombre d'espaces entre les 2 textes.
     * @param decalageVertical Permet de spécifier le nombre de lignes sautées
     * pour le texte à droite.
     * @return Une string avec les deux textes collés.
     */
    public static String jointureDroite(String p1, String p2, int decalageParties, int decalageVertical) {

        // On comble les vides sur la chaine de gauche
        p1 = comblerVides(p1, p2, decalageVertical);

        int largeurMin = trouverLargeurMax(p1);

        String s = "";

        String[] lignesP1 = couperLignes(p1), lignesP2 = couperLignes(p2);

        // on va parcourir ligne par ligne, puis on concatène
        for (int i = 0; i < lignesP1.length; i++) {

            s += lignesP1[i];

            // add s2
            int decalage = largeurMin - lignesP1[i].length();
            for (int d = 0; d < decalage; d++) {
                s += " ";
            }

            for (int totalDecal = 0; totalDecal < decalageParties; totalDecal++) {
                s += " ";
            }

            // On concatène S2
            if (i >= decalageVertical && (lignesP2.length > (i - decalageVertical))) {
                s += lignesP2[i - decalageVertical];
            }

            if (!lignesP1[i].equals(" ")) {
                s += "\n";
            }
        }

        return s;

    }

    /**
     * Permet de trouver la largeur maximale d'une ligne d'un texte.
     *
     * @param texte
     * @return
     */
    public static int trouverLargeurMax(String texte) {
        int max = 0;
        String[] lines = texte.split("\r\n|\r|\n");

        for (String line : lines) {
            max = line.length() > max ? line.length() : max;
        }

        return max;

    }

    /**
     * Retourne le nombre de lignes d'un texte.
     *
     * @param t Le texte.
     * @return Un nombre.
     */
    public static int compterLignes(String t) {
        return couperLignes(t).length;
    }

    /**
     * Retourne un tableau avec toutes les lignes du texte.
     *
     * @param t Un texte.
     * @return
     */
    public static String[] couperLignes(String t) {
        return t.split("\r\n|\r|\n");
    }

    /**
     * Cette méthode permet d'ajouter autant de lignes qu'il faut.
     *
     * @param p1
     * @param p2
     * @return
     */
    private static String comblerVides(String p1, String p2, int decalVerti) {
        int lignesP2 = compterLignes(p2);
        int nbAjouter = lignesP2 - compterLignes(p2) + decalVerti;

        if (nbAjouter <= 0) {
            return p1;
        }

        for (int i = 0; i < nbAjouter; i++) {
            p1 += "\n ";
        }

        return p1;
    }

    /**
     * Active les messages d'erreur en mode de DEBUG.
     * @param msg 
     */
    public static void printErr(String msg) {
        if (Constante.DEBUG_MODE) {
            System.err.println(msg);
        }
    }

}
