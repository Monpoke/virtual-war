package virtualwar.actions;

import outils.InfoTransfert;
import outils.Sons;
import outils.Textes;
import virtualwar.fenetres.Jeu;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Cellule;
import virtualwar.plateau.Case;
import virtualwar.robots.Char;
import virtualwar.robots.Robot;

/**
 * Gère le déplacement pour les robots.
 *
 * @author Square N6
 */
public class Deplacement extends Action {

    /**
     * Constructeur.
     *
     * @param robot
     * @param direction
     */
    public Deplacement(Robot robot, Coordonnees direction) {
        super(robot, direction);
    }

    /**
     * Effectue le déplacement.
     */
    @Override
    public void agit() {
        
        // On vérifie si ce n'est pas une action réseau
        notifierServeur("deplacement");
        boolean directionValide=false; 
         /**
         * On vérifie que le vecteur passé initialement est dans les coordonnées
         * du Robot, on utilise cette mhéthode et non la méthode contains car la méthode contains ne marche pas.
         */
        for (Coordonnees coord : getRobot().getDirectionsPossibles()){
            if(coord.getX()==getDirection().getX() && coord.getY()== getDirection().getY()){
                directionValide=true;
            } 
            
        }

       
        //if (isNetwork() || getRobot().getDirectionsPossibles().contains(getDirection())) {
          if(isNetwork() || directionValide){
            int nbCases = (getRobot() instanceof Char) ? 2 : 1;
            boolean directionTrouvee = false;

            /**
             * On teste les directions. On sait qu'il y a au maximum 2 cases. Du
             * coup, quand il y a deux cases, on teste un obstacle à 2 cases.
             * Puis on divise par deux.
             */
            Coordonnees nouvelleDirection = new Coordonnees(getDirection().getX() / nbCases, getDirection().getY() / nbCases);

            for (int div = nbCases; div >= 1 && !directionTrouvee; div--) {

                Coordonnees coordsCaseToGo = getRobot().getCelluleActuelle().getCoordonnees().ajoutVecteur(nouvelleDirection);
                // ALORS ON Y VA
                if (getRobot().getEquipe().getVue().deplacementOK(getRobot(), coordsCaseToGo)) {
                    // on teste si la case est libre
                    Cellule caseCible = getRobot().getEquipe().getVue().getPlateau().getCellule(coordsCaseToGo);

                    Textes.printErr("Peut se déplacer");

                    // On joue le son de déplacement
                    Sons.jouer("deplacement");

                    // On fait les opérations nécessaires...
                    getRobot().deplacerSur(caseCible);
                    
                    if (getRobot().getCelluleActuelle() instanceof Case && ((Case) getRobot().getCelluleActuelle()).contientMine()) {
                        getRobot().setEnergie(getRobot().getEnergie() - Constante.DEGATS_MINE);
                        getRobot().getCelluleActuelle().setMine(null);

                        // on joue le son d'une mine.
                        Sons.jouer("mineExplose");
                    }

                } else {
                    directionTrouvee = true;
                    Textes.printErr("JE NE PEUX PAS ME DEPLACER !");
                }

            }
        } else {
            Textes.printErr("Un souci de direction est survenu.");
        }

        getRobot().setEnergie(getRobot().getEnergie() - getRobot().getCoutDeplacement());
        // on teste si le robot est allé sur une mine.

    }

}
