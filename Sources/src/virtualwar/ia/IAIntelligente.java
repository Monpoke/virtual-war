/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package virtualwar.ia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import outils.Textes;
import virtualwar.actions.Action;
import virtualwar.actions.Attaque;
import virtualwar.actions.Deplacement;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Case;
import virtualwar.plateau.Cellule;
import virtualwar.robots.Char;
import virtualwar.robots.Robot;

/**
 *
 * @author ludo
 */
public class IAIntelligente extends IA {

    private HashMap<Robot, Coordonnees> robotsPositions = new HashMap<>();

    public IAIntelligente(Equipe equipe) {
        super(equipe);

    }

    @Override
    /*
     On choisit les robots voulu en fonction du nombre de robots.
     */
    public void choisirRobots(int nbRobots) {

        // Ajout de char 
        for (int i = 0; i < nbRobots; i++) {
            getEquipe().ajoutRobot(new Char(getEquipe()));
        }

    }

    /**
     * IA DE Pierre
     *
     * @return
     */
    @Override
    public String jouer() {
        // Dans cette première partie, on analyse le plateau
        HashMap<Robot, HashMap<Robot, Integer>> distancesRobots = this.analyserPlateau();

        HashMap<String, Robot> detecterCible = this.detecterCible(distancesRobots);

        if(detecterCible == null){
            return "Impossible d'agir...";
        }
        
        String detecterAction = this.detecterAction(detecterCible);

        this.sauvegarderCoordonnees();

        return detecterAction;

    }

    /**
     * Cette méthode permet d'analyser les robots sur le plateau
     */
    private HashMap<Robot, HashMap<Robot, Integer>> analyserPlateau() {

        HashMap<Robot, HashMap<Robot, Integer>> robotsDistances = new HashMap<>();
        List<Robot> mesRobots = new ArrayList<>();

        // Get mes robots
        for (Robot robot : this.getEquipe().getRobots()) {
            robotsDistances.put(robot, new HashMap<Robot,Integer>());
            mesRobots.add(robot);
        }

        // foreach equipes
        for (Equipe equi : Jeu.getSJ().getEquipes()) {
            if (equi.equals(this.getEquipe())) {
                continue;
            }

            for (Robot rob : equi.getRobots()) {
                // calcul de la distance entre chaque robots
                int x = rob.getCoordonnees().getX(),
                        y = rob.getCoordonnees().getY();

                Textes.printErr("Robot: " + rob.getIdentifiantReseau() + " " + x + "," + y);

                // Est Sur Base donc on zappe
                if (rob.estSurBase()) {
                    continue;
                }

                // mettre la distance
                for (Robot monRobot : mesRobots) {
                    robotsDistances.get(monRobot).put(rob, this.nbCasesEntreRobots(monRobot.getCoordonnees().getX(), monRobot.getCoordonnees().getY(), x, y, monRobot));
                }
            }

        }

        return robotsDistances;

    }

    /**
     * Retourne le nombre de cases à parcourir entre deux cellules
     *
     * @TODO: Cases diagonales
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    private int nbCasesEntreRobots(int x1, int y1, int x2, int y2, Robot robotSeDeplacant) {
        return (x1 - x2) + (y1 - y2);
    }

    /**
     * On sauvegarde les anciennes coordonnées pour s'y référer plus tard
     */
    private void sauvegarderCoordonnees() {

        // Equipes
        for (Equipe equi : Jeu.getSJ().getEquipes()) {

            // robots
            for (Robot rob : equi.getRobots()) {

                Coordonnees co = new Coordonnees(rob.getCoordonnees().getX(), rob.getCoordonnees().getY());

                this.robotsPositions.put(rob, co);

            }
        }

    }

    /**
     * On détecte la cible de nos robots
     *
     * @param distancesRobots
     */
    private HashMap<String, Robot> detecterCible(HashMap<Robot, HashMap<Robot, Integer>> distancesRobots) {

        // Celui qui possède la distance la plus courte 
        Robot monRobot = null;
        Robot adverse = null;
        int minDistance = -1;

        for (Map.Entry<Robot, HashMap<Robot, Integer>> robot_Eq : distancesRobots.entrySet()) {
            Robot robotEquipe = robot_Eq.getKey();

            for (Map.Entry<Robot, Integer> robotsAdverses : robot_Eq.getValue().entrySet()) {
                Robot robAdverse = robotsAdverses.getKey();
                int distance = robotsAdverses.getValue();

                // on regarde la distance au plus court
                /**
                 * @TODO: vérifier qu'il a assez d'énergie pour se déplacer.
                 */
                if (minDistance == -1 || distance < minDistance) {
                    monRobot = robotEquipe;
                    adverse = robAdverse;
                }
            }
        }

        HashMap<String, Robot> retour = new HashMap<>();
        retour.put("robotIA", monRobot);
        retour.put("adverse", adverse);
        
        if(adverse == null){
            return null;
        }

        return retour;
    }

    /**
     * Tirer ? Miner ? Bouger ?
     *
     * @param detecterCible
     */
    private String detecterAction(HashMap<String, Robot> detecterCible) {

        // On calcule le vecteur de direction du robot adverse
        Coordonnees ad = detecterCible.get("adverse").getCoordonnees();
        Coordonnees mo = detecterCible.get("robotIA").getCoordonnees();
        Coordonnees vecteurDirection = new Coordonnees((ad.getX() - mo.getX()), (ad.getY() - mo.getY()));

        // même direction
        for (Coordonnees actionRayon : detecterCible.get("robotIA").getActionDirectionsPossibles()) {
            
            Coordonnees nc = mo.ajoutVecteur(actionRayon);
            int distance = nbCasesEntreRobots(nc.getX(), nc.getY(), ad.getX(), ad.getY(), detecterCible.get("robotIA"));
            
            if (distance <= 3 && vecteurDirection.estColineaireA(actionRayon) && vecteurDirection.memeDirectionQue(nc)) {
                return tirer(detecterCible, actionRayon);
            }

        }

        return deplacer(detecterCible);
    }

    private boolean vaVersLeHaut(Coordonnees c) {
        return c.getY() < 0;
    }

    private boolean vaVersLeBas(Coordonnees c) {
        return c.getY() > 0;
    }

    private boolean vaVersLaGauche(Coordonnees c) {
        return c.getX() < 0;
    }

    private boolean vaVersLaDroite(Coordonnees c) {
        return c.getX() > 0;
    }

    private String deplacer(HashMap<String, Robot> detecterCible) {
        // on ne fait que se déplacer pour le moment
        List<Coordonnees> mesDirections = detecterCible.get("robotIA").getDirectionsPossibles();
        Coordonnees minDirection = null;
        int distance = -1;

        // robot de l'IA
        Coordonnees robCoo = detecterCible.get("robotIA").getCoordonnees();

        // Robot adverse
        Coordonnees cAdv = detecterCible.get("adverse").getCoordonnees();

        for (Coordonnees direction : mesDirections) {

            Coordonnees nC = robCoo.ajoutVecteur(direction);

            Textes.printErr("robCoo en dessous de : " + robCoo.enDessousDe(cAdv) + " -> " + robCoo.toString() + " - " + cAdv.toString());

            // attrib = false
            boolean attrib = false;

            // on cherche une direction qui convient
            if ((robCoo.enDessousDe(cAdv) && this.vaVersLeHaut(direction))) {
                attrib = true;
            } else if ((!robCoo.enDessousDe(cAdv) && this.vaVersLeBas(direction))) {
                attrib = true;
            } else if ((robCoo.aGaucheDe(cAdv) && this.vaVersLaDroite(direction))) {
                attrib = true;
            } else if ((!robCoo.aGaucheDe(cAdv) && this.vaVersLaGauche(direction))) {
                attrib = true;
            }

            int nDistance = nbCasesEntreRobots(nC.getX(), nC.getY(), cAdv.getX(), cAdv.getY(), detecterCible.get("robotIA"));
            if (attrib == true && (distance == -1 || nDistance < distance)) {
                minDirection = direction;
                distance = nDistance;
            }
        }

        // On empêche les directions nulles
        if (minDirection == null) {
            return "Je ne me déplacerai pas...";
        }

        // let's play
        Action deplacement = new Deplacement(detecterCible.get("robotIA"), minDirection);
        deplacement.agit();

        return "Je me déplace vers " + minDirection.toString() + ".";
    }

    private String tirer(HashMap<String, Robot> detecterCible, Coordonnees direction) {
// let's play
        Action attaque = new Attaque(detecterCible.get("robotIA"), direction);
        attaque.agit();

        return "J'ai tiré vers " + direction.toString() + " !";

    }
}
