package reseau2.socket.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import outils.InfoTransfert;
import outils.Textes;
import virtualwar.equipes.Equipe;
import virtualwar.fenetres.Jeu;
import virtualwar.robots.Robot;

/**
 * On écoute ce que le client peut envoyer.
 *
 * @author Pierre
 */
class EcouterClient implements Runnable {

    private ObjectInputStream in;
    private final Socket socket;
    String message = "";

    InfoTransfert object;

    boolean connexionOk = true;
    private final Client client;

    /**
     * Constructeur.
     *
     * @param in
     * @param socket
     * @param client
     */
    EcouterClient(ObjectInputStream in, Socket socket, Client client) {
        this.in = in;
        this.socket = socket;
        this.client = client;
    }

    @Override
    public void run() {
        while (connexionOk) {
            try {
                Textes.printErr("J'ecoute toujours...");
                try {
                    object = (InfoTransfert) in.readObject();
                    Textes.printErr("J'AI RECU QUELQUE CHOSE!");
                } catch (ClassNotFoundException ex) {
                    System.err.println("Erreur de classe : " + ex.getMessage());
                } catch (EOFException e) {
                    disconnectSocket();
                    connexionOk = false;
                }

                // on vérifie que la connexion est toujours valide.
                if (connexionOk) {
                    Textes.printErr("Message reçu : " + object.getType());
                    object.isLocal(false);
                    
                    // notifie le serveur
                    Jeu.getSJ().getServeur().notifierInfos(object, client);

                }

            } catch (IOException e) {
                Textes.printErr("ERREUR IO:" + e.getMessage());
                disconnectSocket();
                connexionOk = false;
                Textes.printErr("Une connexion a été perdue.");

            }
        }
    }

    /**
     * On détecte les pertes de flux.
     */
    private void disconnectSocket() {
        client.disconnectSocket();
    }

}
