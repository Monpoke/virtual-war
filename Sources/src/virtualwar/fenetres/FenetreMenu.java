/*
 *  Pierre Bourgeois - 2014
 */
package virtualwar.fenetres;

import virtualwar.general.Constante;

/**
 * Cette fenêtre représente le menu principal du jeu.
 *
 * @author Pierre
 */
public class FenetreMenu extends Fenetre {

    public FenetreMenu() {
        super("Menu principal du jeu");

    }

    @Override
    public void lancer() {
        afficheFenetre();

        /**
         * Gérer les choix
         */
        
        String output = "1) Jouer\n";
        output += "2) Partie réseau\n";
        output += "3) Aide\n";
        output += "4) Quitter\n";
        
        String input = "";
        int choixMenuFct;
        if(Constante.debug_IA==true){
            Jeu.demanderParametres();
            return;
        }
        
            do {
                if (input.length() > 0) {
                    afficherErreur("Choisissez une option valide !");
                }
                System.out.println(output);
                System.out.print("Faites un choix : ");
                input = scan.next();

                choixMenuFct = convertStringToInt(input, -1);
                System.out.println();
            } while (!(choixMenuFct >= 1 && choixMenuFct <= 4));

            /**
             * Gère le choix des menus
             */
            switch (choixMenuFct) {
                case 1:
                    Jeu.demanderParametres();
                    break;
                case 2:
                    Jeu.partieReseau();
                    break;
                case 3:
                    Jeu.afficherAide();
                    break;
                default:
                    System.out.println("A bientôt !");
                    System.exit(0);
            }
        }
    }

