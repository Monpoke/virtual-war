package virtualwar.equipes;

import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import reseau2.socket.client.ConnexionClient;
import virtualwar.plateau.Base;
import virtualwar.plateau.Cellule;
import virtualwar.general.Constante;
import virtualwar.robots.Piegeur;
import virtualwar.robots.Robot;

import virtualwar.ia.IA;

/**
 * Cette classe représente une équipe.
 *
 * @author Florent Marcaille, Pierre Bourgeois
 * @since 2015-03-25
 */
public class Equipe implements Serializable {

    /**
     * Numéro d'équipe.
     */
    private final int numero;
    /**
     * Variable automatique.
     */
    private static int numeroAutomatique = 0;

    /**
     * Nom du pays.
     */
    private String pays;

    /**
     * Référence vers la base.
     */
    private Base base;

    /**
     * Robots de l'équipe.
     */
    private final List<Robot> robots;

    /**
     * Vue associée à l'équipe.
     */
    private Vue vue;
    
    /**
     * Paramètres d'IA
     */
    private boolean isIA = false;
    private transient IA ia = null;
    private boolean isJoueurReseau = false;

    // Permet de conserver un lien vers le socket associé.
    private transient Socket socketReseau = null;

    /**
     * Constructeur de l'équipe.
     *
     * @param pays pays d'une equipe
     */
    public Equipe(String pays) {
        this.pays = pays;
        this.numero = numeroAutomatique++;

        robots = new ArrayList<>();
    }

    /**
     * Retourne le numéro d'équipe
     *
     * @return le numéro de l'équipe
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Retourne vrai si c'est un joueur réseau.
     *
     * @return
     */
    public boolean isJoueurReseau() {
        return isJoueurReseau;
    }

    public void setIsJoueurReseau(boolean isJoueurReseau) {
        this.isJoueurReseau = isJoueurReseau;
    }

    /**
     * Retourne le pays de l'équipe
     *
     * @return le pays de l'equipe
     */
    public String getPays() {
        return pays;
    }

    /**
     * Retourne la liste des robots d'une equipe
     *
     * @return la list des robots d'une equipe
     */
    public List<Robot> getRobots() {
        return robots;
    }

    /**
     * Retourne la vue de l'equipe
     *
     * @return la vie de l'equipe
     */
    public Vue getVue() {
        return vue;
    }

    /**
     * Associe une vue à l'équipe.
     *
     * @param vue Vue de l'équipe.
     */
    public void setVue(Vue vue) {
        this.vue = vue;
    }

    /**
     * Ajoute un robot à l'équipe.
     *
     * @param robot
     */
    public void ajoutRobot(Robot robot) {
        robots.add(robot);
        robot.deplacerSur((Cellule) getBase());
        robot.setIdentifiant(robots.size());
    }

    /**
     * Retourne la base de l'équipe.
     *
     * @return une base
     */
    public Base getBase() {
        return base;
    }

    /**
     * Modifie la base de l'équipe.
     *
     * @param base une base.
     */
    public void setBase(Base base) {
        this.base = base;
    }

    /**
     * Compte le nombre de robot d'un certain type.
     *
     * @param type le nom du robot à compter
     * @return
     */
    public int compterTypeRobot(String type) {
        int nb = 0;

        for (Robot robot : robots) {
            if (robot.getType().equals(type)) {
                nb++;
            }
        }

        return nb;
    }

    /**
     * Retourne le nombre de robots opérationnels.
     *
     * @return
     */
    public int getNbRobotsOperationnels() {
        int nb = 0;
        for (Robot robot : robots) {
            if (!robot.estMort()) {
                nb++;
            }
        }
        return nb;

    }

    /**
     * Retourne vrai si c'est une IA.
     *
     * @return
     */
    public boolean isIA() {
        return isIA;
    }

    /**
     * Retourne l'IA
     *
     * @return une instance d'IA
     */
    public IA getIa() {
        return ia;
    }

    /**
     * Attribue une IA
     *
     * @param ia une IA.
     */
    public void setIa(IA ia) {
        this.ia = ia;
        this.isIA = true;
    }

    /**
     * Cette fonction doit être appelée au début de chaque tour. Elle réattribue
     * des points d'énergie aux robots sur la base.
     */
    public void restaurationDuTour() {
        for (Robot robot : robots) {
            if (robot.estSurBase()) {
                robot.rechargerEnergie(Constante.BASE_RECUPERATION_ENERGIE);

                // Charger Mineurs
                if (robot instanceof Piegeur) {
                    ((Piegeur) robot).rechargerMines();
                }
            }
        }
    }

    /**
     * Cette fonction doit être appelée à la fin de chaque tour. Et elle
     * supprime les robots morts.
     */
    public void supprimerLesMorts() {
        for (Robot robot : robots) {
            if (robot.estMort()) {
                // On vide la case où le robot est mort.
                if (!robot.estSurBase() && robot.getCelluleActuelle() != null) {
                    robot.viderSaCase();
                }
            }
        }
    }

    public void setIdJoueurReseau(Socket socketReseau) {
        this.socketReseau = socketReseau;
    }

    public Object getIdJoueurReseau() {
        return socketReseau;
    }

    public void setPays(String nouveauNom) {
        this.pays = nouveauNom;
    }

}
