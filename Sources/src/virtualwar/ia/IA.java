package virtualwar.ia;

import virtualwar.equipes.Equipe;

/**
 * Cette classe représente la classe mère d'une IA. Chaque nouvelle IA devra
 * hériter
 *
 * @author Pierre Bourgeois
 */
public abstract class IA {

    protected Equipe equipe;

    public IA(Equipe equipe) {
        this.equipe = equipe;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }

    public abstract void choisirRobots(int nbRobots);

    /**
     * *
     * Active la méthode de jeu de l'IA.
     *
     * @return une phrase a afficher lors du tour
     */
    public abstract String jouer();

    public String choisirNom() {

        // On prend une entrée au hasard
        return null;
    }
}
