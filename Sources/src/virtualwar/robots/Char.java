
package virtualwar.robots;

import virtualwar.equipes.Equipe;
import java.util.ArrayList;
import java.util.List;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;

/**
 * Représente un char.
 * 
 * @author Florent Marcaille, Pierre Bourgeois
 * @since 2015-03-25
 */
public class Char extends Robot{
    /**
    * class des caract de char
    * 
    * @param equipe    l'equipe du char
    */
    public Char(Equipe equipe) {
        super(equipe);
        setEnergie(Constante.CHAR.get("energie"));
    }
    

    /**
     * return le cout de l'action du char
     * 
     * @return le cout de l'action du char
     */

    @Override
    public int getCoutAction(){
        return Constante.CHAR.get("tirer");
    }

    /**
     * return le cout d'un deplacement d'un char
     * 
     * @return le cout d'un deplacement d'un char
     */

    @Override
    public int getCoutDeplacement(){
        return Constante.CHAR.get("deplacement");
    }
    
    /**
     * return le degat d'un tire d'un char
     * 
     * @return le degat d'un tire d'un char
     */
    @Override
    public int getDegatTir(){
        return Constante.CHAR.get("degat");
    }
    
    /**
     * return le degat d'une mine
     * le char n'ayant pas de mine il ne peut faire de degat d'une ressource dont il ne dispose pas
     * 
     * @return le degat d'une mine
     */
    @Override
    public int getDegatMine(){
        return 0;
    }

    @Override
    public int getPortee() {
        return Constante.CHAR.get("portee");
    }
    

    /**
     * return char
     * 
     * @return char
     */
    @Override
    public String getType(){
        return "char";
    }
    
    /**
     * return la liste des direction que le char a pour se deplacer
     * 
     * @return la liste des direction que le char a pour se deplacer
     */
    @Override
    public List<Coordonnees> getDirectionsPossibles(){
        List<Coordonnees> listeCoordonnees = new ArrayList<>();
        
        listeCoordonnees.add(Constante.dep.get(8));     // CHAR_HAUT
        listeCoordonnees.add(Constante.dep.get(9));     // CHAR_DROITE
        listeCoordonnees.add(Constante.dep.get(10));    // CHAR_BAS
        listeCoordonnees.add(Constante.dep.get(11));    // CHAR_GAUCHE
        
        return listeCoordonnees;
    }

    /**
     * Retourne les vecteurs où l'action peut être effectuée.
     * @return 
     */
    @Override
    public List<Coordonnees> getActionDirectionsPossibles() {
         List<Coordonnees> listeCoordonnees = new ArrayList<>();
        
        listeCoordonnees.add(Constante.dep.get(0));     // HAUT
        listeCoordonnees.add(Constante.dep.get(2));     // DROITE
        listeCoordonnees.add(Constante.dep.get(4));     // BAS
        listeCoordonnees.add(Constante.dep.get(6));     // GAUCHE
        
        return listeCoordonnees;
    }
    
    
    /**
     * On recharge l'énergie du Char.
     *
     * @param recupEnergie l'énergie à ajouter.
     */
    @Override
    public void rechargerEnergie(int recupEnergie) {
        super.rechargerEnergie(recupEnergie);
        if(this.getEnergie() > Constante.CHAR.get("energie")){
            this.setEnergie(Constante.CHAR.get("energie"));
        }
    }

}
