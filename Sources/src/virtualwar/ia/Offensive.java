package virtualwar.ia;

import java.util.ArrayList;

import java.util.Random;
import outils.Textes;
import virtualwar.actions.Action;

import virtualwar.actions.Attaque;
import virtualwar.actions.Deplacement;
import virtualwar.equipes.Equipe;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Case;
import virtualwar.plateau.Cellule;
import virtualwar.robots.Char;
import virtualwar.robots.Robot;
import virtualwar.robots.Tireur;

/**
 *
 * @author ludo
 */
public class Offensive extends IA{

    public Offensive(Equipe equipe) {
        super(equipe);
        
    }

    
    
    @Override
    /*
    On choisit les robots voulu en fonction du nombre de robots.
    */
    public void choisirRobots(int nbRobots) {
       

        // Ajout de char 
        for(int i = 0; i < nbRobots; i++){
            getEquipe().ajoutRobot(new Tireur(getEquipe()));
        }
       
    }

    

    /*
    Fonction à completer
    */
    @Override
    public String jouer() {
    
        
        // on cherche si un robot est à portée de tir  d'un  robot de l'équipe. 
      
           
           
        
        int minVie =1000 ; // représente le minimum de vie des robots à portée.
        Attaque stoAttaque=null; //stocke une attaque possible
        Case stoCase = null; // sert pour transformer une Cellulle en case.
        ArrayList<Robot> robotEnnemie= new ArrayList<>(); // liste de robot ennemie qui peuvent tirer
        
        for ( Robot robot: this.getEquipe().getRobots()) { // on recupère tout les robots de l'équipe
            if(!(robot.estMort())){
                for (Coordonnees coor : robot.peutTirerSurRobot()) { // on recupère les directions vers lequelle il peut tirer 
                   //pour toucher un robot ennemie

                    if(robot.trouverRobotAPortee(coor).getEnergie()-robot.getDegatTir()<0){ 
                    // on tire si le robot ennemie  meut lors de l'attaque du tir
                     new Attaque(robot, coor).agit();


                        return "je tue ce robot: "+robot.trouverRobotAPortee(coor)+" avec ce robot"+robot;

                        // on cherche le robot qui à le moins de vie
                    }else if(robot.trouverRobotAPortee(coor).getEnergie()-robot.getDegatTir()<minVie 
                            && robot.peutAgir()){
                        minVie=robot.trouverRobotAPortee(coor).getEnergie()-robot.getDegatTir();
                        stoAttaque=new Attaque(robot, coor);


                    }
                    if(stoAttaque!=null){
                        stoAttaque.agit();
                        return "je  tire avec ce robot"+stoAttaque.getRobot()+ " sur ce robot"+
                        stoAttaque.getRobot().trouverRobotAPortee(stoAttaque.getDirection())+"\n méthode tir sur robot sans tuer"; // le robot sur lequelle ont tire


                    }
                }
            }
        }
        /*
               // pas de tir possible sur un ennemie.
               // on cherche donc à déplacer un robot qui est à porter de tir d'un robot ennemie.
              
                //double for car tableau à deux dimension
             for(Cellule[] x : this.getEquipe().getVue().getPlateauFiltre()){
                 for (Cellule y : x) {
                     if(stoCase instanceof Case){
                      stoCase= (Case) y;  // robot ennemie 
                      if(stoCase.contientRobot() && stoCase.getRobot().getEquipe()!=getEquipe()){
                          robotEnnemie.add(stoCase.getRobot());
                          
                      }
                     
                 }
                 
                 
             }
            }
             
             Robot robotAlie;
                for (Robot ennemie : robotEnnemie) {
                    for(Coordonnees coor: ennemie.getActionDirectionsPossibles()){ // toute les direction 
                        //vers lequelle le robot peut tirer
                        if(ennemie.trouverRobotAPortee(coor)!=null  
                                && ennemie.trouverRobotAPortee(coor).getEquipe()==getEquipe()){ // on vérifie que ce sont des robots allié
                            // le robot allié peut se faire tirée dessus on le déplace donc d'une case
                            robotAlie=ennemie.trouverRobotAPortee(coor);
                            new Deplacement(robotAlie, new Coordonnees(0,-1)).agit(); // coordonée de test, à changé après
                            return "Je déplace ce robot:"+robotAlie.toString()+"qui se trouve en "+robotAlie.getCoordonnees().toString()+
                                    " et je le déplace dans cette direction: 0,-1 \n méthode déplace robot à porter de tir d'un robot ennemie";
                        }
                        
                        }
                        
                        
                        
                    }
                    */
    // pour le moment on choisi un robot au hasar et on le fait se déplacer
    //on le fait se rapprocher du robot ennemie le plus proche
            Robot stoRobot=null;
            Deplacement stoDeplacement;
                Random random = new Random();
                do{
                 stoRobot=getEquipe().getRobots().get(random.nextInt(getEquipe().getRobots().size()));
                stoDeplacement=new Deplacement(stoRobot,equipe.getVue().robotEnnemiePlusProche(stoRobot));
                stoDeplacement.agit();
                }while((stoRobot.estMort()));
                return "je déplace ce robot "+stoRobot+ " dans cette direction "+stoDeplacement.getDirection()+" \n méthode se rapprocher";

    }
    
}
    

