
import virtualwar.fenetres.Jeu;

/**
 * Ce fichier représente le point d'entrée de l'application.
 *
 * @author Pierre
 */
public class Main {

    /**
     * La méthode qui est lancée par Java lors du démarrage de l'application.
     * @param args 
     */
    public static void main(String[] args) {
        
        /**
         * On appelle le jeu.
         */
        Jeu.lancer();
        
    }

}
