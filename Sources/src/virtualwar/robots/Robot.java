package virtualwar.robots;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.io.Serializable;

import virtualwar.equipes.Equipe;

import java.util.List;
import java.util.Objects;
import java.util.UUID;




import virtualwar.plateau.Base;
import virtualwar.plateau.Cellule;
import virtualwar.general.Constante;
import virtualwar.general.Coordonnees;
import virtualwar.plateau.Case;
import virtualwar.plateau.Plateau;

import javax.imageio.ImageIO;

import outils.Textes;

/**
 * Représente un robot.
 *
 * @author Pierre Bourgeois
 */
public abstract class Robot implements Constante, Serializable {

    private int energie;

    private Equipe equipe;

    private Cellule celluleActuelle;

    private int identifiant = 0;
    
    private String identifiantReseau = "";

    /**
     * constructeur d'un robot
     *
     * @param equipe l'equipe du robot
     */
    public Robot(Equipe equipe) {
        this.equipe = equipe;
        
        identifiantReseau = UUID.randomUUID().toString();
    }

    /**
     * nouvelle cellule du robot
     *
     * @param cellule ancienne cellule du robot
     */
    public void deplacerSur(Cellule cellule) {
        if (celluleActuelle != null) {
            celluleActuelle.enleverRobot(this);
        }
        this.celluleActuelle = cellule;
        cellule.ajouterRobot(this);
    }

    /**
     * Vérifie si le robot peut se déplacer
     *
     * @return vrai si le robot peut se déplacer.
     */
    public boolean peutBouger() {
        return !estMort() && (getEnergie() - getCoutDeplacement()) > 0 && !getDirectionsPossibles().isEmpty();

    }

    /**
     * On vérifie si le robot peut tirer ou miner par rapport au coût de
     * l'action.
     *
     * @return vrai si le robot peut agir.
     */
    public boolean peutAgir() {
        return !estMort() && getEnergie() - getCoutAction() > 0 && !getCelluleActuelle().estBase();
    }

    /**
     * Retourne vrai si le robot est dans la base.
     *
     * @return vrai si le robot est dans la base.
     */
    public boolean estSurBase() {
        return (celluleActuelle != null && celluleActuelle instanceof Base);
    }

    /**
     * Retourne une représentation ASCII du robot.
     *
     * @return Représentation ASCII du robot.
     */
    @Override
    public String toString() {
        if (getEquipe().getNumero() == 1) {
            return "" + Character.toString(getType().charAt(0)).toUpperCase();
        }
        return "" + Character.toString(getType().charAt(0));
    }

    /**
     * return l'energie du robot
     *
     * @return l'energie du robot
     */
    public int getEnergie() {
        return energie;
    }

    /**
     * return la cellule actuelle
     *
     * @return la cellule actuelle
     */
    public Cellule getCelluleActuelle() {
        return celluleActuelle;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public Coordonnees getCoordonnees() {
        return celluleActuelle.getCoordonnees();
    }

    public String getNom() {
        char l = getType().charAt(0);
        String s = Character.toString(l) + getIdentifiant();

        // On monte la casse si l'équipe est égale à la première
        if (this.getEquipe().getNumero() == 1) {
            s = s.toUpperCase();
        }
        return s;
    }

    public void setIdentifiant(int id) {
        this.identifiant = id;
    }

    public void setEnergie(int energie) {
        this.energie = energie;

        if (this.energie < 0) {
            this.energie = 0;
        }
    }

    /**
     * Teste si le robot n'a plus d'énergie.
     *
     * @return Vrai si le robot est mort.
     */
    public boolean estMort() {
        return getEnergie() <= 0;
    }

    public abstract List<Coordonnees> getActionDirectionsPossibles();

    /**
     * return le cout d'un action
     *
     * @return le cout d'un action
     */
    public abstract int getCoutAction();

    /**
     * return le cout d'un deplacement
     *
     * @return le cout d'un deplacement
     */
    public abstract int getCoutDeplacement();

    /**
     * return le degat d'un tir
     *
     * @return le degat d'un tir
     */
    public abstract int getDegatTir();

    /**
     * return le degat d'un tir
     *
     * @return le degat d'un tir
     */
    public abstract int getDegatMine();

    /**
     * return la porté d'un robot
     *
     * @return la porté d'un robot
     */
    public abstract int getPortee();

    /**
     * return le type d'un robot
     *
     * @return le type d'un robot
     */
    public abstract String getType();

    /**
     * return la liste des direction possible d'un deplacement
     *
     * @return la liste des direction possible d'un deplacement
     */
    public abstract List<Coordonnees> getDirectionsPossibles();

    public void rechargerEnergie(int recupEnergie) {
        this.energie += recupEnergie;
    }

    /**
     * Cette fonction est utilisée pour afficher des caractéristiques
     * supplémentaires liés au robot
     *
     * @return
     */
    public String resumeEtat() {
        return (estMort() ? "Détruit" : "");
    }

    /**
     * Vide la case du robot
     */
    public void viderSaCase() {
        if (getCelluleActuelle() != null) {
            this.getCelluleActuelle().videCase();
            this.celluleActuelle = null;
        }
    }

    /**
     * Renvoie les coordonnées (direction) des robot ennemie qui sont à portée de tir de ce robot. 
     * Cette fonction permet de trouver la direction à utliser pour attaque.
     * @return une Array liste de coordonée,
     */
    public ArrayList<Coordonnees> peutTirerSurRobot() {

        ArrayList<Coordonnees> liste = new ArrayList<Coordonnees>();

        int i = 0;

        Plateau plateau = this.getEquipe().getVue().getPlateau();
        int x = this.getCoordonnees().getX();
        int y = this.getCoordonnees().getY();
        Case stoCase;
        for (int dirX = -1; dirX <= 1; dirX++) { // toute les directions possible, Attention tir possible en diagonal 
            
            for (int dirY = -1; dirY <= 1; dirY++) {
                if (!(y == 0 && x == 0)) { // direction valable 
                    x = this.getCoordonnees().getX();
                    y = this.getCoordonnees().getY();

                    do {
                        x = x + dirX;
                        y = y + dirY;
                        i++;
                    } while (plateau.estDansPlateau(new Coordonnees((x), (y))) && // test que les coordonnées de la cellule sont toujours valides
                            !plateau.getCellule(x, y).estBase() && // N'est pas une base
                            !(plateau.getCellule(x, y).contientRobot()) && // vérifie si on trouve le robot
                            !(plateau.getCellule(x, y).contientObstacle()) && //ou si on passe sur un obstacle
                            (i <= this.getPortee())); // vérifie la portée
                    // on a trouver un robot, qui est en x y, ou un obstacle ou un outOfBounds.
                    Coordonnees coordonnees = new Coordonnees(x, y);
                    if (plateau.estDansPlateau(coordonnees) 
                            && !plateau.getCellule(x, y).estBase()
                            && !(plateau.getCellule(x, y).contientObstacle())
                            && plateau.getCellule(x, y).contientRobot()) {
                        stoCase = (Case) plateau.getCellule(x, y); // on cast en Case pour avoir getRobot
                        if (stoCase.getRobot().getEquipe() != this.getEquipe()) {
                            if(DEBUG_MODE){
                                System.out.println(this.toString()+"peut tire dans cette direction:"+new Coordonnees(dirX,dirY)+"pour toucher un ennemie");
                            }
                            liste.add(new Coordonnees(dirX, dirY)); // ajoue les coordonée
                        }
                    }

                }
            }

        }

        return liste;

    }

    /**
     * Trouve le robot qui est à porter de tir en fonction de la direction. Fonction non testée
     *
     * @param direction
     * @return Un robot,seulement ennemi.
     */
    public Robot trouverRobotAPortee(Coordonnees direction) { // ne pas hésiter à changer le nom
        int dirX = direction.getX();
        int dirY = direction.getY();
        int x = getCoordonnees().getX();
        int y = getCoordonnees().getY();
        Plateau plateau = this.getEquipe().getVue().getPlateau();
        int i = 0;
        Case stoCase;

        do {
            x = x + dirX;
            y = y + dirY;
            i++;
        } while (plateau.estDansPlateau(new Coordonnees((x), (y))) && // test que les coordonnées de la cellule sont toujours valides
                !plateau.getCellule(x, y).estBase() && // N'est pas une base
                !(plateau.getCellule(x, y).contientRobot()) && // vérifie si on trouve le robot
                !(plateau.getCellule(x, y).contientObstacle()) && //ou si on passe sur un obstacle
                (i <= this.getPortee())); // vérifie la portée

        // on a trouver un robot, qui est en x y, ou un obstacle ou un outOfBounds.
        if (plateau.estDansPlateau(new Coordonnees(x, y))
                && !(plateau.getCellule(x, y).contientObstacle())
                && plateau.getCellule(x, y).contientRobot()) {
            stoCase = (Case) plateau.getCellule(x, y); // on cast en Case pour avoir getRobot
            if (stoCase.getRobot().getEquipe() != this.getEquipe()) {

                return stoCase.getRobot();
            }
        }

        System.err.println("Robot non trouver ");
        return null;

    }

    public String getIdentifiantReseau() {
        return identifiantReseau;
    }

    public void setIdentifiantReseau(String identifiantReseau) {
        this.identifiantReseau = identifiantReseau;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Robot other = (Robot) obj;
        if (!Objects.equals(this.identifiantReseau, other.identifiantReseau)) {
            return false;
        }
        return true;
    }
    
    
    public void recopieCaracteristiques(Robot robot){
        if(!robot.equals(this)){
            return;
        }
        
    }
    
    public boolean tireOK(Coordonnees coord) {
		// Si les coordonnées ne sont pas dans le plateau.
		if (!this.getEquipe().getVue().coordDansPlateau(coord)) {
			return false;
		}

		// On récupère la cellule
		Cellule cell = this.getEquipe().getVue().getPlateau().getCellule(coord);

		Textes.printErr("Je teste mon attaque !");

		if(this.getType().equals("tireur")){ // Si le robot est un tireur
			if (cell.estBase() || this.getCelluleActuelle().estBase()) { // On ne peut pas taper si dans la base ou sur une base
				return false;
			}
			for(int i = -1;i<=1;i++){
				for(int j = -1;j<=1;j++){
					Coordonnees coord2 = new Coordonnees(i, j); // test les directions
					if(this.getCoordonnees().ajoutVecteur(coord2)==coord && cell.estLibre()){
						return true;
					}
				}
			}
			/*if ((robot.getCelluleActuelle().getCoordonnees().getX()-coord.getX() <= 1 && // Si x est à +-1 du robot
					robot.getCelluleActuelle().getCoordonnees().getX()-coord.getX() >= -1) &&
					(robot.getCelluleActuelle().getCoordonnees().getY()-coord.getY() <= 1 &&    // si y est à +- 1 du robot
					robot.getCelluleActuelle().getCoordonnees().getY()-coord.getY() >= -1) &&
					(robot.getCelluleActuelle().getCoordonnees().getX()-coord.getX() != 0 &&    // si la cellule n'est pas le robot lui meme
					robot.getCelluleActuelle().getCoordonnees().getY()-coord.getY() != 0)){
				return true;
			}*/
		}
		return false;
	}
}
